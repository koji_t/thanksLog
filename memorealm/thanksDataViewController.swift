//
//  thanksDataViewController.swift
//  memorealm
//
//  Created by a_b_c on 2017/10/29.
//  Copyright © 2017年 konjo_ron. All rights reserved.
//

import UIKit
import RealmSwift
import Foundation

class thanksDataViewController: UIViewController,UIPageViewControllerDelegate {
    @IBOutlet weak var sortYearButton: UIButton!
    
    @IBOutlet weak var sortMonthButton: UIButton!
    
    @IBOutlet weak var sortPersonButton: UIButton!
    
    @IBOutlet weak var checkRateLabel: UILabel!
    
    @IBOutlet weak var checkRateGraphView: UIView!
    
    @IBOutlet weak var checkedNumLabel: UILabel!
    
    @IBOutlet weak var unCheckedNumLabel: UILabel!
    
    let imageView = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("thanksDataViewLoaded")
        

        
        
        // Do any additional setup after loading the view.
    }
    
    //ビューが表示される前
    override func viewWillAppear(_ animated: Bool) {
       
        
        super.viewWillAppear(true)
        print("thanksDataViewWillAppear")
    }
    
    
    //ビューが表示された後
    override func viewDidAppear(_ animated: Bool) {
     
        
        print("thanksDataViewDidAppear")
        
        //グラフを表示。ビュー表示後なのは、アニメーションをつけたいため。
        checkRateLoad()

        
        super.viewDidAppear(true)
    }
    
    //ビューが表示された後
    override func viewDidDisappear(_ animated: Bool) {
        
        
        print("thanksDataViewDidDisappear")
        
        //ビューを離れたら、グラフを全部削除する
        self.addedLayers.forEach {
            $0.removeFromSuperlayer()
        }
        
        //数値を初期化
        checkRateLabel.text = "0%"
        checkedNumLabel.text = "0"
        unCheckedNumLabel.text = "0"
        
        super.viewDidDisappear(true)
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //何でソートするかをモーダルに与えるための変数
    var sortingSource:String = "初期値"
    
    //年ソートで数値を返すための変数
    var yearSortedBy:Int = 0
    
    @IBAction func sortYearButton(_ sender: UIButton) {
        sortingSource = "年"
    }
    
    //月ソートで数値を返すための変数
    var monthSortedBy:Int = 0
    
    @IBAction func sortMonthButton(_ sender: UIButton) {
        sortingSource = "月"
    }
    
    //人ソートでidを返すための変数
    var personIdByTimeSortedBy:Double = 0
    
    @IBAction func sortPersonButton(_ sender: UIButton) {
        sortingSource = "人物"
    }
    
    //遷移前の動作
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //モーダルのビューコントローラーを定義
        let selectModalViewController = segue.destination as! selectModalOfThanksListViewController
        selectModalViewController.sortingSource = sortingSource
        selectModalViewController.whereComeFrom = "thanksData1stViewController"
    }
    
    
    
    //数字が増えるアニメーション
    // Timerクラスのインスタンス
    var timer = Timer()
    
    //チェック率
    // 引数設定
    var targetLabel = UILabel()
    var countNumber = Int()
    var targetNumber = Int()
    var addedUnit = String()
    
    func animetedCount(_ timer: Timer!) {
        
        //カウントにプラス1
        countNumber += 1
        
        // ラベルを更新
        targetLabel.text = "\(countNumber)\(addedUnit)"
        
        // 目的の数字になった時の処理
        if countNumber >= targetNumber {
            // タイマーを止める
            timer.invalidate()
            
        }
    }
    
    //チェック数
    // 引数設定
    var targetLabel2 = UILabel()
    var countNumber2 = Int()
    var targetNumber2 = Int()
    
    
    func animetedCount2(_ timer: Timer!) {
        
        //カウントにプラス1
        countNumber2 += 1
        
        // ラベルを更新
        targetLabel2.text = "\(countNumber2)"
        
        // 目的の数字になった時の処理
        if countNumber2 >= targetNumber2 {
            // タイマーを止める
            timer.invalidate()
            return
        }
    }

    //未チェック数
    // 引数設定
    var targetLabel3 = UILabel()
    var countNumber3 = Int()
    var targetNumber3 = Int()
    
    
    func animetedCount3(_ timer: Timer!) {
        
        //カウントにプラス1
        countNumber3 += 1
        
        // ラベルを更新
        targetLabel3.text = "\(countNumber3)"
        
        // 目的の数字になった時の処理
        if countNumber3 >= targetNumber3 {
            // タイマーを止める
            timer.invalidate()
            
        }
    }

    
    //チェック率の計算
    func checkRateLoad (){
        
        
        
        let realm = try! Realm()
        
        
        //すべてのthanksDataと、チェック済みのthanksDataをそれぞれソートする
        let allThanks0 = realm.objects(thanksData.self)
        let checkedThanks0 = realm.objects(thanksData.self).filter("thanksCheck == true")
        
        //年ソート
        var allThanks1 = realm.objects(thanksData.self)
        var checkedThanks1 = realm.objects(thanksData.self)
        if (yearSortedBy != 0) {
            allThanks1 = allThanks0.filter("thanksDateY == %@",yearSortedBy)
            checkedThanks1 = checkedThanks0.filter("thanksDateY == %@",yearSortedBy)
        }else{
            allThanks1 = allThanks0
            checkedThanks1 = checkedThanks0
        }
        
        //月ソート
        var allThanks2 = realm.objects(thanksData.self)
        var checkedThanks2 = realm.objects(thanksData.self)
        if (monthSortedBy != 0) {
            allThanks2 = allThanks1.filter("thanksDateM == %@",monthSortedBy)
            checkedThanks2 = checkedThanks1.filter("thanksDateM == %@",monthSortedBy)
        }else{
            allThanks2 = allThanks1
            checkedThanks2 = checkedThanks1
        }
        
        //人ソート
        var allThanks = realm.objects(thanksData.self)
        var checkedThanks = realm.objects(thanksData.self)
        if (personIdByTimeSortedBy != 0) {
            allThanks = allThanks2.filter("thanksPerson == %@",personIdByTimeSortedBy)
            checkedThanks = checkedThanks2.filter("thanksPerson == %@",personIdByTimeSortedBy)
        }else{
            allThanks = allThanks2
            checkedThanks = checkedThanks2
        }

        //ソートしたものの数をカウント
        let allThanksCount:Double = Double(allThanks.count)
        let checkedThanksCount:Double = Double(checkedThanks.count)
        
        
        
        
        
        //ありがとデータがまだ未入力の場合
        if(allThanksCount == 0){
            print("ありがとデータがねえです")
            self.checkRateLabel.text = "0%"
            checkedNumLabel.text = "0"
            unCheckedNumLabel.text = "0"
            
            //円グラフの描写
            //ビューの指定
            let targetView:UIView = checkRateGraphView
            //中心
            let centerPointOfCheckGraph:[CGFloat] = [targetView.frame.width/2 ,targetView.frame.height/2]
            //半径
            let radiusOfCheckGraph:CGFloat = CGFloat(targetView.frame.width) / 2
            
            
            //未チェックの割合
            drawCircleGraph(
                graphDisplayView: targetView,
                centerPoint: centerPointOfCheckGraph,
                radiusDistance: radiusOfCheckGraph,
                startRound: 0,
                endRound: 2 * pi,
                graphColorHex: "4d4d4d"
            )
            //真ん中の白い円
            drawCircle(
                graphDisplayView: targetView,
                centerPoint: centerPointOfCheckGraph,
                radiusDistance: radiusOfCheckGraph / 2.5,
                graphColorHex: "ffffff"
            )
            
            
            // UIImageViewを定義
            //画像を選ぶ
            imageView.image = UIImage(named:"thanksHeart")
            //画像の色とアルファ値
            imageView.tintColor = UIColor(hex: "e6e6e6")
            //画像サイズを決める
            imageView.frame.size = CGSize(width:radiusOfCheckGraph / 2.3,height:radiusOfCheckGraph / 2.3)
            // 画像の中心を画面の中心に設定
            imageView.center = CGPoint(x:centerPointOfCheckGraph[0], y:centerPointOfCheckGraph[1])
            
            
            // UIImageViewのインスタンスをビューに追加
            checkRateGraphView.addSubview(imageView)
            
            return
        }
        
        //チェック率の数値を定義
        var checkRateDouble = Double()
        var checkRateInt = Int()
        
        checkRateDouble = (checkedThanksCount / allThanksCount)
        
        checkRateInt = Int(checkRateDouble * 100)
        
        
        
        //チェック率数字アニメ表示
        // 引数設定
        targetLabel = checkRateLabel
        countNumber = 0
        targetNumber = checkRateInt
        addedUnit = "%"
        // 0.01秒ごとにupdateLabel()を呼び出す
        timer = Timer.scheduledTimer(
            timeInterval: (0.1 / Double(targetNumber)),//目標タイムをターゲットの数値で割る
            target: self,
            selector: #selector(animetedCount),
            userInfo: nil,
            repeats: true
        )
        
        checkRateLabel.text = "\(checkRateInt)%"
        
        //チェック済み件数数字アニメ表示
        //引数設定
        targetLabel2 = checkedNumLabel
        countNumber2 = 0
        targetNumber2 = Int(checkedThanksCount)
        // 0.01秒ごとにupdateLabel()を呼び出す
        timer = Timer.scheduledTimer(
            timeInterval: (0.1 / checkedThanksCount),
            target: self,
            selector: #selector(animetedCount2),
            userInfo: nil,
            repeats: true
        )
        checkedNumLabel.text = "\(Int(checkedThanksCount))"
        
        //未チェック件数数字アニメ表示
        // 引数設定
        targetLabel3 = unCheckedNumLabel
        countNumber3 = 0
        targetNumber3 = Int(allThanksCount - checkedThanksCount)
        // 0.01秒ごとにupdateLabel()を呼び出す
        timer = Timer.scheduledTimer(
            timeInterval: (0.1 / (allThanksCount - checkedThanksCount)),
            target: self,
            selector: #selector(animetedCount3),
            userInfo: nil,
            repeats: true
        )
        
        unCheckedNumLabel.text = "\(Int(allThanksCount - checkedThanksCount))"
        
        
        
        
        
        
        //円グラフの描写
        //ビューの指定
        let targetView:UIView = checkRateGraphView
        //中心
        let centerPointOfCheckGraph:[CGFloat] = [targetView.frame.width/2 ,targetView.frame.height/2]
        //半径
        let radiusOfCheckGraph:CGFloat = CGFloat(targetView.frame.width) / 2
        
        //チェック済みの割合
        drawCircleGraph(
            graphDisplayView: targetView,
            centerPoint: centerPointOfCheckGraph,
            radiusDistance: radiusOfCheckGraph,
            startRound: 0,
            endRound: 2 * pi * CGFloat(checkRateDouble),
            graphColorHex: "C7243A"
        )
        
        //未チェックの割合
        drawCircleGraph(
            graphDisplayView: targetView,
            centerPoint: centerPointOfCheckGraph,
            radiusDistance: radiusOfCheckGraph,
            startRound: 2 * pi * CGFloat(checkRateDouble),
            endRound: 2 * pi,
            graphColorHex: "4d4d4d"
        )
        //真ん中の白い円
        drawCircle(
            graphDisplayView: targetView,
            centerPoint: centerPointOfCheckGraph,
            radiusDistance: radiusOfCheckGraph / 2.5,
            graphColorHex: "ffffff"
        )
        
        
        // UIImageViewを定義
        //画像を選ぶ
        imageView.image = UIImage(named:"thanksHeart")
        //画像の色とアルファ値
        imageView.tintColor = UIColor(hex: "C7243A").withAlphaComponent(CGFloat(checkRateDouble))
        //画像サイズを決める
        imageView.frame.size = CGSize(width:radiusOfCheckGraph / 2.3,height:radiusOfCheckGraph / 2.3)
        // 画像の中心を画面の中心に設定
        imageView.center = CGPoint(x:centerPointOfCheckGraph[0], y:centerPointOfCheckGraph[1])
        
        
        // UIImageViewのインスタンスをビューに追加
        checkRateGraphView.addSubview(imageView)
        
        
    }
    
    //円グラフの描写に必要なパラメータを定義
    var graphDisplayView = UIView() //表示するビュー
    let pi = CGFloat(Double.pi)//円周率の定義
    var centerPoint = [CGFloat]() //中心
    var radiusDistance = CGFloat() //半径
    var startRound = CGFloat()// 開始の角度
    var endRound = CGFloat()// 終了の角度
    var graphColorHex = String()
    
    
    //削除に使うためのCAShapeLayerの配列を設定
    var addedLayers = [CAShapeLayer]()
    
    func drawCircleGraph(graphDisplayView:UIView,centerPoint:[CGFloat],radiusDistance:CGFloat,startRound:CGFloat,endRound:CGFloat,graphColorHex:String) {
        
        
        //グラフの半角
        let centerOfRadian = (endRound + startRound) / 2
        
        //半径の何分の１か
        let smallRadiusDistance:CGFloat = radiusDistance / 10
        
        //中心から移動した点（文字表示のポイントに使える？）
        let movedPoint:[CGFloat] = [smallRadiusDistance * sin(centerOfRadian),smallRadiusDistance * -cos(centerOfRadian)]
        
        
        let pathGraph = UIBezierPath()
        //起点を決定
        pathGraph.move(to: CGPoint(x:centerPoint[0], y:centerPoint[1]))
        //円弧を決定。
        //スタートと終わりの角度については、(1,0)基準になっているので-90度して基準を(0,1)にしている
        pathGraph.addArc(withCenter: CGPoint(x:centerPoint[0], y:centerPoint[1]), radius: radiusDistance, startAngle: startRound - (pi / 2), endAngle: endRound - (pi / 2), clockwise: true)
        //帰着点（中心に戻る）
        pathGraph.addLine(to: CGPoint(x:centerPoint[0], y:centerPoint[1]))
        
        
        
        let circle = CAShapeLayer()
        //パスの内部を色ぬり
        circle.fillColor = UIColor(hex: graphColorHex).cgColor
        //パスのレンダリング
        circle.path = pathGraph.cgPath
        
        circle.strokeColor = UIColor(hex: "ffffff").cgColor//枠線の色
        circle.lineWidth = 4 //枠線の太さ
        //circle.anchorPoint = CGPoint(x:centerPoint[0], y:centerPoint[1])
        //circle.transform = CATransform3DMakeScale(1.0, 1.0, 0.0)
        
        
        // 本体ViewにLayerを追加
        graphDisplayView.layer.addSublayer(circle)
        
        
        //グラフ削除用のもの。情報を保持
        //参照：https://teratail.com/questions/26422
        self.addedLayers.append(circle)

        
    }
    
    
    func drawCircle(graphDisplayView:UIView,centerPoint:[CGFloat],radiusDistance:CGFloat,graphColorHex:String) {
        
        
        let pathGraph = UIBezierPath()
        //起点を決定
        pathGraph.move(to: CGPoint(x:centerPoint[0], y:centerPoint[1]))
        //円弧を決定。
        //スタートと終わりの角度については、(1,0)基準になっているので-90度して基準を(0,1)にしている
        pathGraph.addArc(withCenter: CGPoint(x:centerPoint[0], y:centerPoint[1]), radius: radiusDistance, startAngle: -(pi / 2), endAngle: 2*pi - (pi / 2), clockwise: true)
        
        
        
        let circle = CAShapeLayer()
        //パスの内部を色ぬり
        circle.fillColor = UIColor(hex: graphColorHex).cgColor
        //パスのレンダリング
        circle.path = pathGraph.cgPath
        
        // 本体ViewにLayerを追加
        graphDisplayView.layer.addSublayer(circle)
        
        
        //グラフ削除用のもの。情報を保持
        //参照：https://teratail.com/questions/26422
        self.addedLayers.append(circle)
        
        
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
