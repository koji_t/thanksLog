//
//  2ndThanksDataViewController.swift
//  memorealm
//
//  Created by a_b_c on 2017/10/31.
//  Copyright © 2017年 konjo_ron. All rights reserved.
//

import UIKit
import RealmSwift

class thanksData2ndViewController: UIViewController,UITableViewDelegate ,UITableViewDataSource{
    
    //前のビュー（チェック率とかの）
    let thanksDataVC: thanksDataViewController = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "thanksDataViewController") as! thanksDataViewController

    @IBOutlet weak var sortYearButton: UIButton!

    @IBOutlet weak var sortMonthButton: UIButton!
    
    @IBOutlet weak var sortCheckButton: UIButton!
    
    
    @IBOutlet weak var personRateGraphView: UIView!
    
    @IBOutlet weak var rankingTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    
    //ビューが表示された後
    override func viewDidAppear(_ animated: Bool) {
        
        
        print("thanksData2ndViewDidAppear")
        
                
        
        //グラフを表示。ビュー表示後なのは、アニメーションをつけたいため。
        personRateLoad()
        
        //グラフ作成メソッドのデータを使うのでグラフ作成後にリロードする。
        self.rankingTableView.reloadData()
        
        
        
        
        super.viewWillAppear(true)
    }
    
  
    
    //ビューが移動された後
    override func viewDidDisappear(_ animated: Bool) {
        
        
        print("thanksData2ndViewDidDisappear")
        
        //ビューを離れたら、グラフを全部削除する
        thanksDataVC.addedLayers.forEach {
            $0.removeFromSuperlayer()
        }
        
        super.viewDidDisappear(true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //何でソートするかをモーダルに与えるための変数
    var sortingSource:String = "初期値"
    
    //年ソートで数値を返すための変数
    var yearSortedBy:Int = 0

    
    @IBAction func sortYearButton(_ sender: UIButton) {
        sortingSource = "年"
    }
    
    //月ソートで数値を返すための変数
    var monthSortedBy:Int = 0
    
    @IBAction func sortMonthButton(_ sender: UIButton) {
        
        sortingSource = "月"
    }
    
    //チェックソートで数値を返すための変数
    var checkSortedBy:Bool? = nil
    
    @IBAction func sortCheckButton(_ sender: UIButton) {
        sortingSource = "チェック別"
    }
    
    
    //遷移前の動作
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //モーダルのビューコントローラーを定義
        let selectModalViewController = segue.destination as! selectModalOfThanksListViewController
        selectModalViewController.sortingSource = sortingSource
        selectModalViewController.whereComeFrom = "thanksData2ndViewController"
    }


    
    
    
    
    //セクション数
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    //セル数
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var cellCount = Int()
        
        if (whoDataSortedByRank.count <= 10){
            
            cellCount = whoDataSortedByRank.count
            
        }else{
            cellCount = 10
        }
        
        return cellCount
    }
    
    //セル表示
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "rankingTableViewCell", for: indexPath)
        
        
        let rankLabel = cell.contentView.viewWithTag(1) as! UILabel
        let iconImageView = cell.contentView.viewWithTag(2)!
        let nameLabel = cell.contentView.viewWithTag(3) as! UILabel
        let percentLabel = cell.contentView.viewWithTag(4) as! UILabel
        
        rankLabel.text = "\(indexPath.row + 1)位"
//        iconImageView.image = UIImage(named: whoDataSortedByRank[indexPath.row].iconImage)
//        iconImageView.tintColor = UIColor(hex: whoDataSortedByRank[indexPath.row].iconColor)
        
        svgFunc.drawSvgOnUIView(targetView:iconImageView,
                                svgPathData:iconData.pathDic[whoDataSortedByRank[indexPath.row].iconImage]!,
                                svgColorHex:whoDataSortedByRank[indexPath.row].iconColor)
        
        nameLabel.text = whoDataSortedByRank[indexPath.row].name
        percentLabel.text = "\(Int(personIdByRateOrder[indexPath.row].value * 100))%"
        
        
        
        return cell
    }
    

    
    //テーブルビューで使うので、外で宣言しておく
    var whoDataSortedByRank = [WhoData]()
    var personIdByRateOrder = [(key:Double,value:Double)]()
    
    func personRateLoad() {
        
        let realm = try! Realm()
        
        //データを整える
        //thanksDataを人のID順に取得
        let thanksDataResult0:Results = realm.objects(thanksData.self).sorted(byKeyPath: "thanksPerson")
        
        //ここでソートをかける
        
        //年
        var thanksDataResult1 = realm.objects(thanksData.self)
        
        if (yearSortedBy != 0){
            thanksDataResult1 = thanksDataResult0.filter("thanksDateY == %@",yearSortedBy)
        }else {
            thanksDataResult1 = thanksDataResult0
        }
        
        //月
        var thanksDataResult2 = realm.objects(thanksData.self)
        
        if (monthSortedBy != 0){
            thanksDataResult2 = thanksDataResult1.filter("thanksDateM == %@",monthSortedBy)
        }else {
            thanksDataResult2 = thanksDataResult1
        }
        
        //チェック
        var thanksDataResult = realm.objects(thanksData.self)
        if(checkSortedBy != nil){
            thanksDataResult = thanksDataResult2.filter("thanksCheck == %@",checkSortedBy!)
        }else{
            thanksDataResult = thanksDataResult2
        }
        
        
        
        
        //データがない場合の回避用
        if (thanksDataResult.count == 0){
            
            print("データがないので帰ってくれ")
            return
        }
        
        
        //personIDだけの配列を作る
        var personIdDuplicatedArray = [Double]()
        for thanksDataIndividual in thanksDataResult {
            personIdDuplicatedArray.append(thanksDataIndividual.thanksPerson)
        }
        //重複のないpersonIDのArrayを作る
        //sortしないと、順番の要素を持たない集合Set型のままになる。
        let personIdArray = Set(personIdDuplicatedArray).sorted(by: >)
        
        //personIdArrayの各割合を記すためのArray
        var personIdRateArray = [Double]()
        
        for personId in personIdArray {
            let personIdCount:Int = personIdDuplicatedArray.filter{$0 == personId}.count
            let personIdRate:Double = Double(personIdCount) / Double(thanksDataResult.count)
            personIdRateArray.append(personIdRate)
        }
        
        
        //2つのデータを辞書型に格納する
        var personIdAndRateData = [Double:Double]()
        
        for times in 0...personIdArray.count-1 {
            //辞書型にそれぞれを追加する
            personIdAndRateData[(personIdArray[times])] = personIdRateArray[times]
        }
        
        
        // valueで降順ソート。辞書型データを多い順に並べる。keyとvalueのタプル配列となる。
        personIdByRateOrder = personIdAndRateData.sorted(by: {$0.1 > $1.1 })
        
        
        //ここで、この配列順にpersonIDからWhoDataを並べる作業
        //すでに宣言済みのwhoDataSortedByRankを初期化して加えていく
        
        whoDataSortedByRank = [WhoData]()
        
        for personIdRate in personIdByRateOrder {
            let whoDataIndividual:WhoData = realm.objects(WhoData.self).filter("idByTime == %@",personIdRate.key)[0]
            whoDataSortedByRank.append(whoDataIndividual)
        }
        
        
        
        
        //円グラフの描写
        //ビューの指定
        let targetView:UIView = personRateGraphView
        //中心
        let centerPointOfCheckGraph:[CGFloat] = [targetView.frame.width/2 ,targetView.frame.height/2]
        //半径
        let radiusOfCheckGraph:CGFloat = CGFloat(targetView.frame.width) / 2
        //円周率の定義
        let pi = CGFloat(Double.pi)
        
        //グラフ描く。要素分繰り返し
        
        //繰り返し数。つまり人数を得る。最大１０人まで。
        
        var graphDisplayMember = personIdArray.count
        
        if (graphDisplayMember > 10){
            graphDisplayMember = 10
        }
        
        //繰り返し用。次のスタート位置を格納する変数。初期値はゼロ
        var nextStart:CGFloat = 0
        
        for times in 0...(graphDisplayMember - 1) {
         
                thanksDataVC.drawCircleGraph(
                    graphDisplayView: targetView,
                    centerPoint: centerPointOfCheckGraph,
                    radiusDistance: radiusOfCheckGraph,
                    startRound: nextStart,
                    endRound: nextStart + 2 * pi * CGFloat(personIdByRateOrder[times].value),
                    graphColorHex: whoDataSortedByRank[times].iconColor
                )
                
                //startRoundにendRoundの値を加えて次のスタート地点として取得しなおす
                nextStart += 2 * pi * CGFloat(personIdByRateOrder[times].value)
                
            
        }
        
        //その他用
        thanksDataVC.drawCircleGraph(
            graphDisplayView: targetView,
            centerPoint: centerPointOfCheckGraph,
            radiusDistance: radiusOfCheckGraph,
            startRound: nextStart,
            endRound: (2 * pi),
            graphColorHex: "cccccc"
        )
        
        
        

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
