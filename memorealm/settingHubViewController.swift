//
//  settingHubViewController.swift
//  memorealm
//
//  Created by a_b_c on 2017/11/03.
//  Copyright © 2017年 konjo_ron. All rights reserved.
//

import UIKit
import GoogleMobileAds

class settingHubViewController: UIViewController {

    @IBOutlet weak var pathDisplayView: UIView!
    
    @IBOutlet weak var thanksAllDeleteButton: UIButton!
    
    @IBOutlet weak var initializeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool){
        goWhy = ""
        super.viewWillAppear(true)
        
        
        //アドセンス用
        var admobView = GADBannerView()
        
        admobView = GADBannerView(adSize:kGADAdSizeBanner)
        admobView.frame.origin = CGPoint(x:0, y:self.view.frame.size.height - admobView.frame.height - adsence.tabBarHeight)
        admobView.frame.size = CGSize(width:self.view.frame.width, height:admobView.frame.height)
        
        if adsence.AdMobTest {
            admobView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        }
        else{
            admobView.adUnitID = adsence.AdMobID
        }
        
        admobView.rootViewController = self
        admobView.load(GADRequest())
        
        self.view.addSubview(admobView)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var goWhy:String = ""
    
    
    @IBAction func thanksAllDeleteButton(_ sender: UIButton) {
        goWhy = "thanksAllDelete"
        self.performSegue(withIdentifier: "toAllDelConfirmModal", sender: nil)
        
    }
    
    
    @IBAction func initializeButton(_ sender: UIButton) {
        goWhy = "initialization"
        self.performSegue(withIdentifier: "toAllDelConfirmModal", sender: nil)
    }
    
    
    //遷移前の動作
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(goWhy == "thanksAllDelete" || goWhy == "initialization"){
            let allDelModalViewController = segue.destination as! allDelConfirmModalViewController
            allDelModalViewController.whatToDo = goWhy
        }
        
    }

    
    
    
    
    
//    func pathDraw() {
//        let pathGraph = UIBezierPath()
//        
//        
//        let charasujiBoySVG = CAShapeLayer(pathString: pathDatas.charasujiBoy)
//        
//        let circle = CAShapeLayer()
//        //パスの内部を色ぬり
//        circle.fillColor = UIColor(hex: colors.checkedHeart).cgColor
//        //パスのレンダリング
//        circle.path = pathGraph.cgPath
//        
//        // 本体ViewにLayerを追加
//        pathDisplayView.layer.addSublayer(circle)
//    
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
