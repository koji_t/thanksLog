//
//  confirmModalViewController.swift
//  memorealm
//
//  Created by a_b_c on 2017/11/07.
//  Copyright © 2017年 konjo_ron. All rights reserved.
//

import UIKit
import RealmSwift

class confirmModalViewController: UIViewController {
    
    var whoToDelete = WhoData()
    
    @IBOutlet weak var iconImageView: UIView!
    
    @IBOutlet weak var personNameLabel: UILabel!
    
    @IBOutlet weak var deleteOKButton: UIButton!
    
    @IBOutlet weak var deleteCancelButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        iconImageView.image = UIImage(named: "\(whoToDelete.iconImage)")
//        iconImageView.tintColor = UIColor(hex: "\(whoToDelete.iconColor)")
        
        svgFunc.drawSvgOnUIView(targetView:iconImageView,
                                svgPathData:iconData.pathDic[whoToDelete.iconImage]!,
                                svgColorHex:whoToDelete.iconColor)
        
        personNameLabel.text = whoToDelete.name

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func deleteOKButton(_ sender: UIButton) {
        
        let realm = try! Realm()
        
        //その人のありがとを特定しておく
        let whoToDeleteID = whoToDelete.idByTime
        let thanksToDelete = realm.objects(thanksData.self).filter("thanksPerson == %@",whoToDeleteID)
        
        
        //選ばれた人とそのありがとを削除する
        try! realm.write {
            realm.delete(thanksToDelete)
            realm.delete(whoToDelete)
        }
        
        
        //リスト順に並べ直し。
        let whoSortedByListNum = realm.objects(WhoData.self).sorted(byKeyPath: "listNum")
        
        //順番にリスト番号を振り直す
        for times in 0...(whoSortedByListNum.count - 1) {
            try! realm.write {
                whoSortedByListNum[times].listNum = times
            }
        }
        
        
        //前のviewを定義
        let preViewController = self.presentingViewController as! personListForEditViewController
        preViewController.editPersonCollectionView.reloadData()
        
        
        //listNumbersを刷新
        preViewController.listNumbers = []
        let personCount = realm.objects(WhoData.self).count
        for times in 0...(personCount - 1) {
            preViewController.listNumbers.append(times)
        }
        
        
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func deleteCancelButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
