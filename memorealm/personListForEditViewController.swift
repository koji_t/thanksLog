//
//  personListForEditViewController.swift
//  memorealm
//
//  Created by a_b_c on 2017/11/03.
//  Copyright © 2017年 konjo_ron. All rights reserved.
//

import UIKit
import RealmSwift

class personListForEditViewController:UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate{
    
    
    var listNumbers = [Int]()
    
    @IBOutlet weak var backButton: UIButton!
    
    
    @IBOutlet weak var editPersonCollectionView: UICollectionView!
    
    
    @IBOutlet weak var goEditButton: UIButton!

    @IBOutlet weak var reorderButton: UIButton!
    
    @IBOutlet weak var deleteButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("personListForEditVCDidLoad")
        
        // ロングプレスを定義。必須！
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(personListForEditViewController.handleLongGesture(gesture:)))  //Swift3
        
        //longPressGesture.minimumPressDuration = 0.2  //3秒間以上押された場合にロングプレスとする
        //longPressGesture.allowableMovement = 30  //ロングプレスを判定する指が動いていい範囲、単位はpx
        
        self.view.addGestureRecognizer(longPressGesture)
        
        //リストナンバー作成
        let realm = try! Realm()
        
        let personCount = realm.objects(WhoData.self).count
        
        if(personCount != 0) {
            for times in 0...(personCount - 1) {
                listNumbers.append(times)
            }
        
        }else {
            listNumbers = []
        }
      

        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        print("backButonTapped")
        dismiss(animated: true, completion: nil)
    }
    
    
    //セルの表示
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        
        // testCellを定義はストーリーボードで設定したセルのID
        let testCell:UICollectionViewCell =
            collectionView.dequeueReusableCell(withReuseIdentifier: "editViewPersonCell",
                                               for: indexPath)
        
        //WhoDataをlistNumでソート
        let realm = try! Realm()
        let members = realm.objects(WhoData.self).sorted(byKeyPath:"listNum")
        
        // そのcellのWhoDataを抜き出してmemberと定義
        let member = members[indexPath.row]
        
        
        // Tag番号を使ってLabelのインスタンス生成 cell内のlabelを定義
        let nameDisplay = testCell.contentView.viewWithTag(2) as! UILabel
        nameDisplay.text = member.name
        
        
        
        // Tag番号を使ってImageViewのインスタンス生成 testCellのイメージビューを定義
        let whoIcon = testCell.contentView.viewWithTag(1)!
        
//        let cellImage = UIImage(named: "\(member.iconImage)")
//        
//        whoIcon.image = cellImage
//        
//        whoIcon.tintColor = UIColor(hex: "\(member.iconColor)")
        
        svgFunc.drawSvgOnUIView(targetView:whoIcon,
                                svgPathData:iconData.pathDic[member.iconImage]!,
                                svgColorHex:member.iconColor)
        
        
        return testCell
    }
    
    //セクション数
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    //セル数
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        
        // メンバーの数をカウントして、セルの数をその数に設定する
        let realm = try! Realm()
        let members = realm.objects(WhoData.self)
        
        
        return members.count;
    }
    
    var selectedPerson = WhoData()
    var selectPersonOrNot = Bool()
    
    
    
    // Cell がタップされた場合
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        
        if(reorderMode == false){
            // cellをtestCellと定義
            let testCell:UICollectionViewCell = collectionView.cellForItem(at: indexPath)!
            
            testCell.backgroundColor = UIColor(hex: "ffcccc")
            
            let realm = try! Realm()
            
            //listNumで整列
            let members = realm.objects(WhoData.self).sorted(byKeyPath:"listNum")
            
            //その中でタップされたcellのものをmemberと定義
            let member = members[indexPath.row]
            
            //そいつをselectedPersonに入れる
            selectedPerson = member
            
            //人物を選んだかにチェック
            selectPersonOrNot = true
            
            print("タップindexpath",indexPath)
            print(member.name,member.listNum)
        
        
        }
    }
    
    
    //セルの選択が解除されたとき
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if(reorderMode == false){
            let testCell:UICollectionViewCell = collectionView.cellForItem(at: indexPath)!
            testCell.backgroundColor = UIColor(hex: "ffffff")
        }
    }
    
    
    
    //セルのドラッグ＆ドロップ
    //参照：https://qiita.com/eKushida/items/78d53d365bb69c7e7e0e
    //参照2：http://nshint.io/blog/2015/07/16/uicollectionviews-now-have-easy-reordering/
    
    //セルをドロップした時の処理。これがないと動かない。
    func collectionView(_ collectionView: UICollectionView,
                        moveItemAt sourceIndexPath: IndexPath,
                        to destinationIndexPath: IndexPath) {
        
        print("sourceIndexPath.row",sourceIndexPath.row)
        print("sourceIndexPath.item",sourceIndexPath.item)
        print("destinationIndexPath",destinationIndexPath)
        print("destinationIndexPath.item",destinationIndexPath.item)
        
        if(reorderMode == true){
            //numbers[sourceIndexPath.item]をリムーブ。
            let tempNumber = listNumbers.remove(at: sourceIndexPath.item)
            print(listNumbers)
            //リムーブした番号を、numbers[destinationIndexPath.item]に割り込ませる。
            listNumbers.insert(tempNumber, at: destinationIndexPath.item)
            print(listNumbers)

        }
    }
    
    //セルのドラッグ&ドロップを検知するやーつ。必須。
    func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        
        if(reorderMode == true){
            
            switch gesture.state {
                
                
            case .began:
                //Get a reference to the indexPath being pressed
                guard let selectedIndexPath = self.editPersonCollectionView.indexPathForItem(at: gesture.location(in: self.editPersonCollectionView)) else { break }
                
                //Start interactive movement
                self.editPersonCollectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
                print(editPersonCollectionView.indexPathForItem(at: gesture.location(in: self.editPersonCollectionView))!)
                
            case .changed:
                //Update position based on new gesture location
                self.editPersonCollectionView.updateInteractiveMovementTargetPosition(gesture.location(in: self.editPersonCollectionView))
                
            case .ended:
                //End interaction with cell. This will cause moveItemAtIndexPath to fire.
                self.editPersonCollectionView.endInteractiveMovement()
                
            default:
                //Just to cover your bases
                self.editPersonCollectionView.cancelInteractiveMovement()
                
            }
        }
        
    }
    
    
    
    
    
    
    //編集ボタン
    var comeFrom = String()
    @IBAction func goEditButton(_ sender: UIButton) {
        print("goEditButtonTapped")
        
        if(selectPersonOrNot == true){
        
          comeFrom = "personListForEditView"
          self.performSegue(withIdentifier: "toPersonEditView", sender: nil)
        }else{
            print("人を選んでからにしてくださいよ")
        }
    }
    
    //遷移前の動作
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(comeFrom == "personListForEditView"){
            //ありがと編集のビューコントローラーを定義
            let personEditViewController = segue.destination as! WhoAddViewController
            personEditViewController.whereComeFrom = comeFrom
            personEditViewController.whoToEdit = selectedPerson
        }else if(comeFrom == "personListForEditViewTodeleteModal"){
            //削除確認モーダルを定義
            let personEditViewController = segue.destination as! confirmModalViewController
            personEditViewController.whoToDelete = selectedPerson
        }

    }

    
    
    
    
    var reorderMode:Bool = false
    
    @IBAction func reorderButton(_ sender: UIButton) {
        
        print("reorderButtonTapped")
        
        if(reorderMode == false){
            reorderMode = true
            print("reorderModeOn")
            print(reorderMode)
            reorderButton.setTitle("決定する", for: .normal)
            
            //backボタンの無効化
            backButton.isEnabled = false
            
            //編集ボタンの無効化
            goEditButton.isEnabled = false
            
            //削除ボタンの無効化
            deleteButton.isEnabled = false
            
            //選んだ人を消す
            selectedPerson = WhoData()
            print(selectedPerson)
            
            //人物を選んだかにチェック
            selectPersonOrNot = false
        }else{
            //WhoDataの並び順情報を刷新
            
            print("oldListNumbers",listNumbers)
            
            //listNumbersを0から順に取り出してそのIntに該当するWhoDataに1から順の番号をつけていく
            let realm = try! Realm()
            var whoDataArrayByNewOrder = [WhoData]()
            for times in 0...(listNumbers.count - 1) {
                let currentWho = realm.objects(WhoData.self).filter("listNum == %@",listNumbers[times])[0]
                
                //一旦、whoDataの新しい並び順のリストを作る
                whoDataArrayByNewOrder.append(currentWho)
                
            
            }
            
            //作ったwhoDataのリスト順に新しいリストナンバーを振っていく
            for times in 0...(whoDataArrayByNewOrder.count - 1) {
                try! realm.write() {
                    whoDataArrayByNewOrder[times].listNum = times
                }
                
            }
            
            
            //listNumbersを刷新
            listNumbers = []
            let personCount = realm.objects(WhoData.self).count
            for times in 0...(personCount - 1) {
                listNumbers.append(times)
            }
            
            print("newListNumbers",listNumbers)
            
            editPersonCollectionView.reloadData()
            
            
            
            reorderMode = false
            print("reorderModeOff")
            reorderButton.setTitle("並べ替え", for: .normal)
            
            //backボタンの有効化
            backButton.isEnabled = true
            
            //編集ボタンの有効化
            goEditButton.isEnabled = true
            
            //削除ボタンの有効化
            deleteButton.isEnabled = true
        }
    }
    
    
    @IBAction func deleteButton(_ sender: UIButton) {
        
        print("deleteButtonTapped")
        
        if(selectPersonOrNot == true){
            
            comeFrom = "personListForEditViewTodeleteModal"
            self.performSegue(withIdentifier: "toDeleteModal", sender: nil)
        }else{
            print("人を選んでからにしてくださいよ")
        }
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
