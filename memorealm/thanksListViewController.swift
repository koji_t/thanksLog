//
//  thanksListViewController.swift
//  memorealm
//
//  Created by a_b_c on 2017/10/22.
//  Copyright © 2017年 konjo_ron. All rights reserved.
//

import UIKit
import RealmSwift
import GoogleMobileAds

class thanksListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var thanksListTableView: UITableView!
    
    @IBOutlet weak var sortByYearButton: UIButton!
    
    @IBOutlet weak var sortByMonthButton: UIButton!
    
    @IBOutlet weak var sortByPersonButton: UIButton!
    
    @IBOutlet weak var sortByCheckButton: UIButton!
    
    
    //テーブルビューを表示したときに、そのセクションとセル情報を保持するための多重配列
    
    var cellsInSection = [Results<thanksData>?]()
    
    
    //セクション用の日付数値配列を定義
    var thanksDateList:Array = [Int]()
    
    
    //遷移先判定用
    
    var whereToGo = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //現在の日付を定義
        let nowDate = Date()
        
        //カレンダー情報を変数定義
        var calendar = Calendar.current
        
        //カレンダーのロケールを端末のものにする
        calendar.locale = Locale.current
        
        //初期ソート
        yearSortedBy = calendar.component(.year, from: nowDate)
        monthSortedBy = calendar.component(.month, from: nowDate)
        
        //初期ソート表示
        self.sortByYearButton.setTitle("\(yearSortedBy)年", for: .normal)
        self.sortByMonthButton.setTitle("\(monthSortedBy)月", for: .normal)
        self.sortByPersonButton.setTitle("みんな", for: .normal)
        self.sortByCheckButton.setTitle("すべて", for: .normal)

        // Do any additional setup after loading the view.
        
        print("thanksListViewLoaded")
        

    }
    
    //ビューが表示されるとき
    override func viewWillAppear(_ animated: Bool) {
        //テーブルビューをリロード
        
        tableViewReload()
        
        super.viewWillAppear(true)
        print("thanksListViewWillAppear")
        
        
        
        //アドセンス用
        var admobView = GADBannerView()
        
        admobView = GADBannerView(adSize:kGADAdSizeBanner)
        admobView.frame.origin = CGPoint(x:0, y:self.view.frame.size.height - admobView.frame.height - adsence.tabBarHeight)
        admobView.frame.size = CGSize(width:self.view.frame.width, height:admobView.frame.height)
        
        if adsence.AdMobTest {
            admobView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        }
        else{
            admobView.adUnitID = adsence.AdMobID
        }
        
        admobView.rootViewController = self
        admobView.load(GADRequest())
        
        self.view.addSubview(admobView)
        
    }
    
    //ビューを離れるとき
    override func viewWillDisappear(_ animated: Bool) {
        //テーブルビューをリロード
        
        thanksCheckChangeDo()
        
        super.viewWillAppear(true)
        print("thanksListViewWillDisappear")
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //リロード用ファンクション
    func tableViewReload() {
        
        
        //まず、ありがとチェックの変更をデータに反映させる
        thanksCheckChangeDo()
        
        
        //realm宣言
        let realm = try! Realm()
        
        
        //テーブルビューに表示させる前に、どのように表示させるかをcellsInSectionという配列に
        //入れ込んでいる。テーブルビューには、cellsInSectionをデータにして表示させる。
        
        //一度cellInSectionをまっさらにする。
        cellsInSection = [Results<thanksData>?]()
        
        
        //thanksDataのthanksDateIntの降順でsortしたものをthanksDateList0と定義
        let thanksDataDateList0 = realm.objects(thanksData.self).sorted(byKeyPath:"thanksDateInt",ascending: false)
        //ifで使うために変数を定義しておく
        var thanksDataDateList1 = realm.objects(thanksData.self)
        var thanksDataDateList2 = realm.objects(thanksData.self)
        var thanksDataDateList3 = realm.objects(thanksData.self)
        var thanksDataDateList = realm.objects(thanksData.self)
        
        //年ソート（強制）
        thanksDataDateList1 = thanksDataDateList0.filter("thanksDateY == %@",yearSortedBy)
        
        //月ソート
        if (monthSortedBy != 0){
            //monthSortedByが初期値じゃない場合。月でソートする。
            thanksDataDateList2 = thanksDataDateList1.filter("thanksDateM == %@", monthSortedBy)
        }else{
            //月でソートしない場合は値はそのまま。
            thanksDataDateList2 = thanksDataDateList1
        }
        
        //人ソート
        if (personIdByTimeSortedBy != 0){
            //personIdByTimeSortedByが初期値じゃない場合。人でソートする。
            thanksDataDateList3 = thanksDataDateList2.filter("thanksPerson == %@", personIdByTimeSortedBy)
        }else{
            //人でソートしない場合は値をそのままに。
            thanksDataDateList3 = thanksDataDateList2
        }
        
        //チェックソート
        if (checkSortedBy == true){
            //チェック済みソート。
            thanksDataDateList = thanksDataDateList3.filter("thanksCheck == true")
        }else if(checkSortedBy == false){
            //チェック済みソート。
            thanksDataDateList = thanksDataDateList3.filter("thanksCheck == false")
        }else{
            //チェックでソートしない場合は値をそのままに。
            thanksDataDateList = thanksDataDateList3
        }
        
        //日付数列だけの配列を定義
        var thanksDateArray:Array = [Int]()
        //thanksDataDateListの日付数列だけを配列にする
        for thanksDateIndividual in thanksDataDateList {
            thanksDateArray.append(thanksDateIndividual.thanksDateInt)
        }
        //できた配列の重複を消したものを降順にソート。これがセクションと同じ配列になる。
        thanksDateList = Set(thanksDateArray).sorted(by: >)
        
        
        if (thanksDateList == []){
            print(thanksDateList,"ありがとデータなし")
        }else{
            // セクション回数分の繰り返し処理（0からセクションカウント-1まで）
            //-1しているのは、例えばセクションが4つの場合には0,1,2,3のやつが欲しいから
            for someThing in 0...(thanksDateList.count - 1) {
                
                //セクション分のResult?をつくる。空の要素を入れておく
                cellsInSection.append(nil)
                
                //セクションの番手ごとに定義
                let thanksDateIntIndependent = thanksDateList[someThing]
                
                //日付数列と同じものをid順に並べる
                let thanksDataInSection0 = realm.objects(thanksData.self).filter("thanksDateInt == %@",thanksDateIntIndependent).sorted(byKeyPath:"thanksIdByTime")
                
                //if文で使うために定義しておく
                var thanksDataInSection1 = realm.objects(thanksData.self)
                var thanksDataInSection2 = realm.objects(thanksData.self)
                var thanksDataInSection3 = realm.objects(thanksData.self)
                var thanksDataInSection = realm.objects(thanksData.self)
                
                //年ソート（強制）
                thanksDataInSection1 = thanksDataInSection0.filter("thanksDateY == %@",yearSortedBy)
                
                //月ソート
                if (monthSortedBy != 0){
                    //monthSortedByが初期値じゃない場合。月でソートする。
                    thanksDataInSection2 = thanksDataInSection1.filter("thanksDateM == %@", monthSortedBy)
                }else{
                    //月でソートしない場合は値はそのまま。
                    thanksDataInSection2 = thanksDataInSection1
                }

                //人ソート
                if (personIdByTimeSortedBy != 0){
                    //personIdByTimeSortedByが初期値じゃない場合。
                    thanksDataInSection3 = thanksDataInSection2.filter("thanksPerson == %@", personIdByTimeSortedBy)
                }else{
                    //人でソートしない場合は値はそのまま。
                    thanksDataInSection3 = thanksDataInSection2
                }
                
                //チェックソート
                if (checkSortedBy == true){
                    //チェック済みソート。
                    thanksDataInSection = thanksDataInSection3.filter("thanksCheck == true")
                }else if(checkSortedBy == false){
                    //チェック済みソート。
                    thanksDataInSection = thanksDataInSection3.filter("thanksCheck == false")
                }else{
                    //チェックでソートしない場合は値をそのままに。
                    thanksDataInSection = thanksDataInSection3
                }


                cellsInSection[someThing] = thanksDataInSection
            }
        
        }
        
        
        //一番上までスクロール
        self.thanksListTableView.contentOffset = CGPoint(x:0,y:0)
        
                //リロード
        self.thanksListTableView.reloadData()
        
        
               
        print("tableViewReloaded")

    }
    
    
    //テーブルビュー
    // Section数
    func numberOfSections(in tableView: UITableView) -> Int {
        
        
        return thanksDateList.count
    }
    // Sectionのタイトル
    func tableView(_ tableView: UITableView,
                   titleForHeaderInSection section: Int) -> String? {
        //realm宣言
        let realm = try! Realm()
        
        //thanksDateListの中でセクションの番手に該当するものを定義
        let thanksDateCurrent = thanksDateList[section]
        
        //thanksDataのthanksDateIntがthanksDateCurrentと一致するもの（の0番目）をthanksDataDateForHeaderと定義
        let thanksDataDateForHeader = realm.objects(thanksData.self).filter("thanksDateInt == %@",thanksDateCurrent)[0]
        
        //thanksDataDateForHeaderの年月日を取得してString化
        let thanksSectionHeader:String = "\(thanksDataDateForHeader.thanksDateY)年\(thanksDataDateForHeader.thanksDateM)月\(thanksDataDateForHeader.thanksDateD)日(\(thanksDataDateForHeader.thanksDateWD))"
        
        
        
        return thanksSectionHeader
    }
    
    
    //セルに表示する文字列を設定する
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //セルを定義
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "thanksListCell", for: indexPath)
        
        //realm宣言
        let realm = try! Realm()
        
        
        // cellの順番にあたるものを抜き出してthankと定義
        let currentSection = cellsInSection[indexPath.section]
        let thank = currentSection![indexPath.row]
        
        //誰
        //まずは、そのセルに当てはまるthanksPersonIDを変数で定義
        let thanksPersonID = thank.thanksPerson
        
        //thanksPersonIDと同じ人物をWhoDataから探す。性質上必ず一人に絞られるはずなので0番目をとる。
        let selectedPerson = realm.objects(WhoData.self).filter("idByTime == %@",thanksPersonID)
        let identifiedPerson = selectedPerson[0]
        
        //タグ1のイメージビューに該当人物のアイコンの画像と色を入れ込む
        let personIconDisplay = cell.contentView.viewWithTag(1)!
//        personIconDisplay.image = UIImage(named: "\(identifiedPerson.iconImage)")
//        personIconDisplay.tintColor = UIColor(hex: "\(identifiedPerson.iconColor)")
        
        svgFunc.drawSvgOnUIView(targetView:personIconDisplay,
                                svgPathData:iconData.pathDic[identifiedPerson.iconImage]!,
                                svgColorHex:identifiedPerson.iconColor)
        
        
        //該当人物名をタグ2のラベルにぶちこむ
        let personNameDisplay = cell.contentView.viewWithTag(2) as! UILabel
        personNameDisplay.text = identifiedPerson.name
        
        
        //イベント
        // 該当のありがとイベントをタグ3のラベルにぶちこむ
        let eventDisplay = cell.contentView.viewWithTag(3) as! UILabel
        eventDisplay.text = thank.thanksEvent
        
        //チェック
        //タグ4のボタンの色をありがとチェックの判定で変える
        
        let thanksCheckDisplay = cell.contentView.viewWithTag(4) as! UIButton
        if(thank.thanksCheck == true){
            thanksCheckDisplay.tintColor = UIColor(hex: colors.checkedHeart)
        }else{
            thanksCheckDisplay.tintColor = UIColor(hex: colors.unCheckedHeart)
        }
        
        
        return cell
    }
    
    
    
    //セル数
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        let currentSection = cellsInSection[section]
        
        
        return currentSection!.count
    }
    
    
    
    //タップされたthanks情報を格納する変数
    var thanksForEditting = thanksData()
    
    //セルタップ時
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let currentSection = cellsInSection[indexPath.section]
        thanksForEditting = currentSection![indexPath.row]
        
        whereToGo = "thanksListViewForEditting"
        
        
        self.performSegue(withIdentifier: "toThanksEditView", sender: nil)
    }

    
    
    //左スワイプで削除
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            let currentSection = cellsInSection[indexPath.section]
            let thank = currentSection![indexPath.row]
            
            
            let realm = try! Realm()
            
            try! realm.write {
                realm.delete(thank)
            }
            
            //セクション内がゼロでelse内の処理を行うとクラッシュするので、リロードに変えて逃す
            if (currentSection!.count == 0){
                
                tableViewReload()
            }else{
                
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            
            
        }
    }
    
    
    
    
    //クリックしたハートの情報を保持するための変数
    var thanksCheckChangedData = [thanksData]()
    
    @IBAction func thanksCheckButton(_ sender: UIButton) {
        
        
        //押されたボタンのセルを定義
        let cell = sender.superview?.superview as! UITableViewCell
        //そのセルのセクション番号を取得して定義
        let sectionRow:Int! = thanksListTableView.indexPath(for:cell)?.section
        //そのセルのセル番号を取得して定義
        let cellRow:Int! = thanksListTableView.indexPath(for:cell)?.row
        
        //セクションとセルの番号で押されたセルの情報を特定する
        let currentSection = cellsInSection[sectionRow]
        let currentThanksData = currentSection![cellRow]
        //true,falseでif,データを逆転して更新
        if (sender.tintColor == UIColor(hex: colors.unCheckedHeart)){
            
            sender.tintColor = UIColor(hex: colors.checkedHeart)

        }else{
            
            sender.tintColor = UIColor(hex: colors.unCheckedHeart)
  
        }
        
        //タップしたthanksDataを入れ込む
        thanksCheckChangedData.append(currentThanksData)
        
        //なぜタップした時点で更新させないのかというと
        //たとえば、『チェック済み』でソートした状態でデータを更新させてしまうと
        //tableViewの表示（セル数など）がおかしくなってクラッシュしてしまうため。
        
    }
    
    
    //ありがとチェックの変更をデータに反映させる
    func thanksCheckChangeDo() {
        //realm宣言
        let realm = try! Realm()
        
        //リロードの前に、タップされたデータを元にチェックを変更する
        for currentData in thanksCheckChangedData {
            if(currentData.thanksCheck == true){
                try! realm.write {
                    currentData.thanksCheck = false
                }
            }else{
                try! realm.write {
                    currentData.thanksCheck = true
                }
            }
        }
        
        //使ったデータをまっさらにしておく
        thanksCheckChangedData = [thanksData]()

    }
    
    
    
    //何でソートするかをモーダルに与えるための変数
    var sortingSource:String = "初期値"
    
    //年ソートで数値を返すための変数
    var yearSortedBy = Int()
    //年ソートボタン
    @IBAction func sortByYearButton(_ sender: UIButton) {
        sortingSource = "年"
        whereToGo = "selectModal"
    }
    
    //月ソートで数値を返すための変数
    var monthSortedBy:Int = 0
    //月ソートボタン
    @IBAction func sortByMonthButton(_ sender: UIButton) {
        sortingSource = "月"
        whereToGo = "selectModal"
    }
    
    //人ソートでidを返すための変数
    var personIdByTimeSortedBy:Double = 0
    //人ソートボタン
    @IBAction func sortByPersonButton(_ sender: UIButton) {
        
        sortingSource = "人物"
        whereToGo = "selectModal"
    }
    
    
    //チェック別ソートで数値を返すための変数
    var checkSortedBy:Bool? = nil
    //チェック別ソートボタン
    @IBAction func sortByCheckButton(_ sender: UIButton) {
        
        sortingSource = "チェック別"
        whereToGo = "selectModal"
    }
    
    //遷移前の動作
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(whereToGo == "selectModal"){
        //モーダルのビューコントローラーを定義
        let selectModalViewController = segue.destination as! selectModalOfThanksListViewController
            selectModalViewController.sortingSource = sortingSource
        }else if(whereToGo == "thanksListViewForEditting"){
            //ありがと編集のビューコントローラーを定義
            let thanksEditViewController = segue.destination as! ViewController
            thanksEditViewController.whereComeFrom = whereToGo
            thanksEditViewController.selectedThanks = thanksForEditting
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}



class selectModalOfThanksListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var selectTableView: UITableView!
    
    //前のビューと共通の変数
    var sortingSource = String()
    let sortListArray:Array = [Double]()
    var whereComeFrom = String()
    var whereComeFrom2 = UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        print("sortingListViewLoaded")
        print(sortingSource)
        
        self.selectTableView.reloadData()
        
        
    }

    
    //モーダルのバックグラウンドをタップしたら戻る処理。正直まだファンクション理解してない。
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        for touch: UITouch in touches {
            let tag = touch.view!.tag
            if tag == 1 {
                dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    //セクション数
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //セル数
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var cellCount = 1
        
        if (sortingSource == "年"){
            let realm = try! Realm()
            
            let thanksDataSortedByYear = realm.objects(thanksData.self).sorted(byKeyPath: "thanksDateY")
            
            //年だけの配列を定義
            var thanksYearArray:Array = [Int]()
            //hankasDataSortedByYearの年だけを配列にする
            for thanksYearIndividual in thanksDataSortedByYear {
                thanksYearArray.append(thanksYearIndividual.thanksDateY)
            }
            //できた配列の重複を消したものを昇順にソート。
            thanksYearArray = Set(thanksYearArray).sorted()
            
            if (whereComeFrom == "thanksData1stViewController" || whereComeFrom == "thanksData2ndViewController") {
                
                //すべての年でソートしたいので
                return thanksYearArray.count + 1
            
            }
            
            cellCount = thanksYearArray.count

        }else if (sortingSource == "月"){
            
            cellCount = 13
            
        }else if (sortingSource == "人物"){
            let realm = try! Realm()
            
            let whoDataSort = realm.objects(WhoData.self).sorted(byKeyPath: "idByTime")
            
            //+1 は、『みんな』枠のため
            cellCount = whoDataSort.count + 1
            
        }else if(sortingSource == "チェック別"){
            cellCount = 3
        }else{
        }

        
        
        return cellCount
    }
    
    //セル表示
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "selectTableViewCell", for: indexPath)
        //セル内のlabelを定義
        let displayLabel = cell.contentView.viewWithTag(1) as! UILabel
        
        if (sortingSource == "年"){
            let realm = try! Realm()
            
            let thanksDataSortedByYear = realm.objects(thanksData.self).sorted(byKeyPath: "thanksDateY")
            
            //年だけの配列を定義
            var thanksYearArray:Array = [Int]()
            //hankasDataSortedByYearの年だけを配列にする
            for thanksYearIndividual in thanksDataSortedByYear {
                thanksYearArray.append(thanksYearIndividual.thanksDateY)
            }
            //できた配列の重複を消したものを降順にソート。
            thanksYearArray = Set(thanksYearArray).sorted(by: >)
            
            
            if (whereComeFrom == "thanksData1stViewController" || whereComeFrom == "thanksData2ndViewController") {
                
                //『すべての年』を入れるので
                
                if (indexPath.row == 0){
                    displayLabel.text = "すべての年"
                    return cell
                }
                
                displayLabel.text = "\(thanksYearArray[indexPath.row - 1])年"
                return cell
                
            }
            
            displayLabel.text = "\(thanksYearArray[indexPath.row])年"
            
            
        }else if (sortingSource == "月"){
            
            if (indexPath.row == 0){
                //最初のセルはすべての月とする
                displayLabel.text = "すべての月"
            }else{
                displayLabel.text = "\(indexPath.row)月"
            }
            
        }else if (sortingSource == "人物"){
            if (indexPath.row == 0){
                //『みんな』枠
                displayLabel.text = "みんな"
            }
            else{
                
                let realm = try! Realm()
                
                let whoDataSort = realm.objects(WhoData.self).sorted(byKeyPath: "idByTime")
                
                let displayedPerson = whoDataSort[indexPath.row - 1]
                
                displayLabel.text = displayedPerson.name
            }
            
            
            
        }else if(sortingSource == "チェック別"){
            if(indexPath.row == 0){
                displayLabel.text = "すべて"
            }else if(indexPath.row == 1){
                displayLabel.text = "チェック済み"
            }else{
                displayLabel.text = "未チェック"
            }
            
        }else{
        }
        
        return cell
    }
    
    
    
    //セルタップ時
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        //戻り先のビューを定義。戻り先がタブバーの一員なので、
        //まずタブバーを定義して、その何番目のタグのビューかを定義する
        let tabCtrl = self.presentingViewController as! UITabBarController
        
        let thanksListVC = tabCtrl.viewControllers![1] as! thanksListViewController
        
        //ページビューからの遷移の場合のモーダル表示前のビューを定義。タブバーの3番目を指定。
        let pageVC = tabCtrl.viewControllers![2] as! UIPageViewController
        
        //ページビューに表示されるVCはページビューを読み込んだときに定義される。
        //よって、アプリを起動してからページビューを読み込む前に以下の操作をするとクラッシュしてしまう。
        //それを回避するために、ページビューを読み込んでいるかのifをかましている。
        var thanksData1stVC = thanksDataViewController()
        var thanksData2ndVC = thanksData2ndViewController()
        if(pageVC.viewControllers! != [] && whereComeFrom == "thanksData1stViewController"){
            //ページビューの0番目を指定。
            //正直これで死ぬほど詰まったけど、この書き方でOKっぽい。
            thanksData1stVC = pageVC.viewControllers![0] as! thanksDataViewController

        }else if(pageVC.viewControllers! != [] && whereComeFrom == "thanksData2ndViewController"){
          
            //ページビューの0番目。変な感じだけど、ページビューはこういう構造らしい。
            thanksData2ndVC = pageVC.viewControllers![0] as! thanksData2ndViewController
            
        }
        
        
        
        
        if (sortingSource == "年"){
            let realm = try! Realm()
            
            let thanksDataSortedByYear = realm.objects(thanksData.self).sorted(byKeyPath: "thanksDateY")
            
            //年だけの配列を定義
            var thanksYearArray:Array = [Int]()
            //hankasDataSortedByYearの年だけを配列にする
            for thanksYearIndividual in thanksDataSortedByYear {
                thanksYearArray.append(thanksYearIndividual.thanksDateY)
            }
            //できた配列の重複を消したものを昇順にソート。
            thanksYearArray = Set(thanksYearArray).sorted(by: >)
            
            if (whereComeFrom == "thanksData1stViewController") {
                
                
                //『すべての年』を入れるのでその場合分け
                if (indexPath.row == 0){
                    
                    
                    //値を渡す
                    thanksData1stVC.yearSortedBy = 0
                    
                    //ボタンタイトル変更
                    thanksData1stVC.sortYearButton.setTitle("すべての年", for: .normal)
                    
                }else{
                    //値を渡す
                    thanksData1stVC.yearSortedBy = thanksYearArray[indexPath.row - 1]
                    
                    
                    //ボタンタイトル変更
                    thanksData1stVC.sortYearButton.setTitle("\(thanksData1stVC.yearSortedBy)年", for: .normal)
                
                }
                
                //チェック率をリロード
                thanksData1stVC.checkRateLoad()
                
                dismiss(animated: true, completion: nil)
                
                return
                
                
            }
            
            if (whereComeFrom == "thanksData2ndViewController") {
                
                
                //『すべての年』を入れるのでその場合分け
                if (indexPath.row == 0){
                    
                    
                    //値を渡す
                    thanksData2ndVC.yearSortedBy = 0
                    
                    //ボタンタイトル変更
                    thanksData2ndVC.sortYearButton.setTitle("すべての年", for: .normal)
                    
                }else{
                    //値を渡す
                    thanksData2ndVC.yearSortedBy = thanksYearArray[indexPath.row - 1]
                    
                    //ボタンタイトル変更
                    thanksData2ndVC.sortYearButton.setTitle("\(thanksData2ndVC.yearSortedBy)年", for: .normal)
                    
                }
                
                //チェック率をリロード
                thanksData2ndVC.personRateLoad()
                
                //ランキングをリロード
                thanksData2ndVC.rankingTableView.reloadData()
                
                dismiss(animated: true, completion: nil)
                
                return
                
                
            }


            
            
            //値を渡す
            thanksListVC.yearSortedBy = thanksYearArray[indexPath.row]
            
            //戻り先を更新
            thanksListVC.tableViewReload()
            
            
            //ボタンタイトル変更
            thanksListVC.sortByYearButton.setTitle("\(thanksListVC.yearSortedBy)年", for: .normal)
            
            dismiss(animated: true, completion: nil)
            
            return

            
            
        }else if (sortingSource == "月"){
            
            
            if (whereComeFrom == "thanksData1stViewController") {
                
                if (indexPath.row == 0){
                    //最初のセルはすべての月
                    
                    //値を渡す。初期化。
                    thanksData1stVC.monthSortedBy = 0
                    
                    
                    //ボタンタイトル変更
                    thanksData1stVC.sortMonthButton.setTitle("すべての月", for: .normal)
                    
                    
                    
                }else{
                    //値を渡す。
                    thanksData1stVC.monthSortedBy = indexPath.row
                    
                    
                    //ボタンタイトル変更
                    thanksData1stVC.sortMonthButton.setTitle("\(thanksData1stVC.monthSortedBy)月", for: .normal)
                }
                
                //戻り先を更新
                thanksData1stVC.checkRateLoad()
                
                dismiss(animated: true, completion: nil)
                
                
                return
            
            }else if (whereComeFrom == "thanksData2ndViewController") {
                
                if (indexPath.row == 0){
                    //最初のセルはすべての月
                    
                    //値を渡す。初期化。
                    thanksData2ndVC.monthSortedBy = 0
                    
                    
                    //ボタンタイトル変更
                    thanksData2ndVC.sortMonthButton.setTitle("すべての月", for: .normal)
                    
                    
                    
                }else{
                    //値を渡す。
                    thanksData2ndVC.monthSortedBy = indexPath.row
                    
                    
                    //ボタンタイトル変更
                    thanksData2ndVC.sortMonthButton.setTitle("\(thanksData2ndVC.monthSortedBy)月", for: .normal)
                }
                
                //戻り先を更新
                thanksData2ndVC.personRateLoad()
                
                //ランキングをリロード
                thanksData2ndVC.rankingTableView.reloadData()
                
                dismiss(animated: true, completion: nil)
                
                
                return
                
            }else{
                
                if (indexPath.row == 0){
                    //最初のセルはすべての月
                    
                    //値を渡す。初期化。
                    thanksListVC.monthSortedBy = 0
                    
                    
                    //ボタンタイトル変更
                    thanksListVC.sortByMonthButton.setTitle("すべての月", for: .normal)
                    
                    
                    
                }else{
                    //値を渡す。
                    thanksListVC.monthSortedBy = indexPath.row
                    
                    
                    //ボタンタイトル変更
                    thanksListVC.sortByMonthButton.setTitle("\(thanksListVC.monthSortedBy)月", for: .normal)
                }
                
                //戻り先を更新
                thanksListVC.tableViewReload()
                
                dismiss(animated: true, completion: nil)
                
                return
            
            }

            
        }else if (sortingSource == "人物"){
            
            if (whereComeFrom == "thanksData1stViewController") {
                
                if (indexPath.row == 0){
                    //『みんな』枠
                    //値を渡す。初期化。
                    thanksData1stVC.personIdByTimeSortedBy = 0
                    
                    
                    //戻り先を更新
                    thanksData1stVC.checkRateLoad()
                    
                    //ボタンタイトル変更
                    thanksData1stVC.sortPersonButton.setTitle("みんな", for: .normal)
                    
                    
                    
                }else{
                    let realm = try! Realm()
                    
                    let whoDataSort = realm.objects(WhoData.self).sorted(byKeyPath: "idByTime")
                    
                    //値を渡す
                    thanksData1stVC.personIdByTimeSortedBy = whoDataSort[indexPath.row - 1].idByTime
                    
                    
                    //ソートする人の名前を取得する
                    let sortingPersonName = realm.objects(WhoData.self).filter("idByTime == %@",thanksData1stVC.personIdByTimeSortedBy)[0].name
                    
                    
                    //ボタンタイトル変更
                    thanksData1stVC.sortPersonButton.setTitle("\(sortingPersonName)", for: .normal)
                }
                
                //戻り先を更新
                thanksData1stVC.checkRateLoad()
                
                
                dismiss(animated: true, completion: nil)
                
                return

            
            }
            
            
            
            if (indexPath.row == 0){
                //『みんな』枠
                //値を渡す。初期化。
                thanksListVC.personIdByTimeSortedBy = 0
                
                //戻り先を更新
                thanksListVC.tableViewReload()
                
                //ボタンタイトル変更
                thanksListVC.sortByPersonButton.setTitle("みんな", for: .normal)
                
                
                
                
            }else{
                let realm = try! Realm()
                
                let whoDataSort = realm.objects(WhoData.self).sorted(byKeyPath: "idByTime")
                
                //値を渡す
                thanksListVC.personIdByTimeSortedBy = whoDataSort[indexPath.row - 1].idByTime
                
                
                //ソートする人の名前を取得する
                let sortingPersonName = realm.objects(WhoData.self).filter("idByTime == %@",thanksListVC.personIdByTimeSortedBy)[0].name
                
                //ボタンタイトル変更
                thanksListVC.sortByPersonButton.setTitle("\(sortingPersonName)", for: .normal)
            }
            
            //戻り先を更新
            thanksListVC.tableViewReload()
            
            dismiss(animated: true, completion: nil)
            
            return
            
        }else if(sortingSource == "チェック別"){
            
            if (whereComeFrom == "thanksData2ndViewController") {
                
                if(indexPath.row == 0){
                    //値を渡す
                    thanksData2ndVC.checkSortedBy = nil
                    
                    //ボタンタイトル変更
                    thanksData2ndVC.sortCheckButton.setTitle("すべて", for: .normal)
                    
                    
                }else if(indexPath.row == 1){
                    //値を渡す
                    thanksData2ndVC.checkSortedBy = true
                    
                    //ボタンタイトル変更
                    thanksData2ndVC.sortCheckButton.setTitle("チェック済み", for: .normal)
                    
                }else{
                    //値を渡す
                    thanksData2ndVC.checkSortedBy = false
                    
                    
                    //ボタンタイトル変更
                    thanksData2ndVC.sortCheckButton.setTitle("未チェック", for: .normal)
                    
                }
                
                //戻り先を更新
                thanksData2ndVC.personRateLoad()
                
                //ランキングをリロード
                thanksData2ndVC.rankingTableView.reloadData()
                
                dismiss(animated: true, completion: nil)
                
                return
            }
            
            if(indexPath.row == 0){
                //値を渡す
                thanksListVC.checkSortedBy = nil
                
                //ボタンタイトル変更
                thanksListVC.sortByCheckButton.setTitle("すべて", for: .normal)
                

            }else if(indexPath.row == 1){
                //値を渡す
                thanksListVC.checkSortedBy = true
                
                
                //ボタンタイトル変更
                thanksListVC.sortByCheckButton.setTitle("チェック済み", for: .normal)
                
            }else{
                //値を渡す
                thanksListVC.checkSortedBy = false
                
                
                //ボタンタイトル変更
                thanksListVC.sortByCheckButton.setTitle("未チェック", for: .normal)
                
            }
            
            //戻り先を更新
            thanksListVC.tableViewReload()
            
            dismiss(animated: true, completion: nil)
            
            return
            
        }else{
            return
        }
    }

    
}
