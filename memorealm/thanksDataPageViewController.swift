//
//  thanksDataPageViewController.swift
//  memorealm
//
//  Created by a_b_c on 2017/10/31.
//  Copyright © 2017年 konjo_ron. All rights reserved.
//

import UIKit
import GoogleMobileAds

class thanksDataPageViewController: UIPageViewController,UIPageViewControllerDataSource,UIPageViewControllerDelegate {
    
    //参照：http://mypace1981.doorblog.jp/archives/49614586.html
    
    
    
    
    let sboard: UIStoryboard? = UIStoryboard(name:"Main", bundle:nil)
    
    var pageViewControllers = [UIViewController]()
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //自分自身を指定
        dataSource = self
        
        //1ページ目をインスタンス化
        let firstThanksDataViewController: thanksDataViewController = sboard!.instantiateViewController(withIdentifier: "thanksDataViewController") as! thanksDataViewController
        
        //2ページ目をインスタンス化
        let secondThanksDataViewController: thanksData2ndViewController = sboard!.instantiateViewController(withIdentifier: "thanksData2ndViewController") as! thanksData2ndViewController
        
        //全ページを配列に格納
        pageViewControllers = [firstThanksDataViewController,secondThanksDataViewController]
        //UIPageViewControllerに表示対象を設定
        setViewControllers([pageViewControllers[0]], direction: .forward, animated: false, completion: nil)
        
        
        print("pageViewLoaded")
        
        
        
        
        
        
        // Configure our custom pageControl
        
        
        
        

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        //アドセンス用
        var admobView = GADBannerView()
        
        admobView = GADBannerView(adSize:kGADAdSizeBanner)
        admobView.frame.origin = CGPoint(x:0, y:self.view.frame.size.height - admobView.frame.height - adsence.tabBarHeight)
        admobView.frame.size = CGSize(width:self.view.frame.width, height:admobView.frame.height)
        
        if adsence.AdMobTest {
            admobView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        }
        else{
            admobView.adUnitID = adsence.AdMobID
        }
        
        admobView.rootViewController = self
        admobView.load(GADRequest())
        
        self.view.addSubview(admobView)
        
    }

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func pageViewController(_ pageViewController:
        UIPageViewController, viewControllerBefore viewController:UIViewController) -> UIViewController? {
        print("swipedRight")
        
        //firstPageApper()
        
        //右にスワイプした場合に表示したいviewControllerを返す
        //ようはページを戻す
        //今表示しているページは何ページ目か取得する
        let index = pageViewControllers.index(of: viewController)
        if index == 0 {
            //1ページ目の場合は何もしない
            print("ButPage1")
            
            return nil
        } else {
            //1ページ目意外の場合は1ページ前に戻す
            print("thenGoBack")
            return pageViewControllers[index!-1]
        }
    }
    
    func pageViewController(_ pageViewController:
        UIPageViewController, viewControllerAfter viewController: UIViewController) ->
        UIViewController? {
            print("swipedleft")
            
            
            //左にスワイプした場合に表示したいviewControllerを返す
            //ようはページを進める
            //今表示しているページは何ページ目か取得する
            let index = pageViewControllers.index(of: viewController)
            if index == pageViewControllers.count-1 {
                //最終ページの場合は何もしない
                print("ButPage2")
                
                
                
                return nil
            } else {
                //最終ページ意外の場合は1ページ進める
                print("thenGoNext")
                
                             
                return pageViewControllers[index!+1]
            }
    }
    
    //全ページ数を返すメソッド
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return pageViewControllers.count
    }
    
    //ページコントロールの最初の位置を返すメソッド
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
