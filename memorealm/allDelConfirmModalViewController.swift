//
//  allDelConfirmModalViewController.swift
//  memorealm
//
//  Created by a_b_c on 2017/11/07.
//  Copyright © 2017年 konjo_ron. All rights reserved.
//

import UIKit
import RealmSwift

class allDelConfirmModalViewController: UIViewController {
    
    @IBOutlet weak var warningLabel: UILabel!
    
    @IBOutlet weak var OKButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if(whatToDo == "thanksAllDelete"){
            warningLabel.text = "ありがとログをすべて削除します"
            
        
            
        }else if (whatToDo == "initialization"){
            warningLabel.text = "初期化します"
        }
            
    


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var whatToDo:String = ""
    
    
    @IBAction func OKButton(_ sender: UIButton) {
        let realm = try! Realm()
        let allThanks = realm.objects(thanksData.self)
        let allPerson = realm.objects(WhoData.self)
        
        
        if(whatToDo == "thanksAllDelete"){
            
            try! realm.write{
                realm.delete(allThanks)
            }
            
        }else if (whatToDo == "initialization"){
            try! realm.write{
                realm.delete(allThanks)
                realm.delete(allPerson)
            }

        }
        
        let tabCtrl = self.presentingViewController as! UITabBarController
        
        let preVC = tabCtrl.viewControllers![3] as! settingHubViewController
        
        preVC.goWhy = ""
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButton(_ sender: UIButton) {
        
        let tabCtrl = self.presentingViewController as! UITabBarController
        
        let preVC = tabCtrl.viewControllers![3] as! settingHubViewController
        
        preVC.goWhy = ""
        
        dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
