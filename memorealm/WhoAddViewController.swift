//
//  WhoAddViewController.swift
//  memorealm
//
//  Created by a_b_c on 2017/10/18.
//  Copyright © 2017年 konjo_ron. All rights reserved.
//

import UIKit
import RealmSwift

struct iconData{
    static let pathDic:[String:String] = ["standard":pathDatas.standard,
                                          "mummy":pathDatas.mummy]
}


class WhoAddViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate{
    
    
    // アイコン画像の名前を配列。この情報で並べる。
    let iconNames:[String] = ["standard","mummy"]
    let colorHex:[String] = ["4D4D4D","007AB7","EDAD0B","D6407C","A4C520","C7243A","B61972"]
    
    
    // 画像の状態の変数を定義
    var selectedImage = ""
    var selectedColor = ""
    
    
    //どこから来たの変数
    var whereComeFrom:String = ""
    //誰を編集するの変数
    var whoToEdit = WhoData()

    
    
    @IBOutlet weak var addWhoName: UITextField!

    @IBOutlet weak var iconPreView: UIView!
    
    @IBOutlet weak var addWhoRegister: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    
    
    @IBOutlet weak var genocideButton: UIButton!
    
    @IBOutlet weak var whoAddChoseImageCollectionView: UICollectionView!
    
    @IBOutlet weak var whoAddChoseColorCollectionView: UICollectionView!
    
    
    
    //削除に使うためのCAShapeLayerの配列を設定
    var addedLayers = [CAShapeLayer]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if(whereComeFrom == ""){
            //初期表示アイコン画像、色ともに配列の最初の値をぶちこむ
            selectedImage = iconNames[0]
            selectedColor = colorHex[0]
//
//            iconPreImageView.image = UIImage(named: selectedImage)
//            iconPreImageView.tintColor = UIColor(hex: selectedColor)
            
            
            svgFunc.drawSvgOnUIView(targetView:iconPreView,
                                     svgPathData:iconData.pathDic[selectedImage]!,
                                     svgColorHex:selectedColor)
            
            
            
        
        }else if(whereComeFrom == "personListForEditView"){
            addWhoName.text = whoToEdit.name
            selectedImage = whoToEdit.iconImage
            selectedColor = whoToEdit.iconColor
            
//            iconPreImageView.image = UIImage(named: selectedImage)
//            iconPreImageView.tintColor = UIColor(hex: selectedColor)
            
            
            svgFunc.drawSvgOnUIView(targetView:iconPreView,
                                     svgPathData:iconData.pathDic[selectedImage]!,
                                     svgColorHex:selectedColor)
            
            
        }
        
        
        // キーボードに表示するツールバーの表示
        let keyBoardToolBar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.size.height/6,width: self.view.frame.size.width,height: 40.0))
        keyBoardToolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
        keyBoardToolBar.isTranslucent = false
        keyBoardToolBar.tintColor = UIColor.white
        keyBoardToolBar.barTintColor = UIColor(hex: "c7243a")
        
        //ボタンの設定
        //右寄せのためのスペース設定
        let spaceBarBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace,target: self,action:#selector(ViewController.keyBoardToolBarBtnPush))
        
        //完了ボタンを設定
        let toolBarDoneButton = UIBarButtonItem(title: "完了", style: .done, target: self, action: #selector(ViewController.keyBoardToolBarBtnPush))
        
        
        //ツールバーにボタンを表示
        keyBoardToolBar.items = [spaceBarBtn,toolBarDoneButton]
        addWhoName.inputAccessoryView = keyBoardToolBar

        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        

    }
    
    // キーボードを閉じるためのaction
    @IBAction func addWhoName(_ sender: UITextField) {
    }
    
    
    //キーボードの完了ボタンを押した時の処理
    func keyBoardToolBarBtnPush(sender: UIBarButtonItem){
        
        //キーボードを閉じる
        self.view.endEditing(true)
        
    }

        
    
    
    //アイコン、カラー選択コレクションビュー
    
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        // cellひとつひとつが行う処理
        
        var testCell = UICollectionViewCell()
        
        if( collectionView.tag == 1 ){
            // 画像選択コレクションビュー（タグ1）
            // testCellと定義 "whoAddChoseImageCell"" はストーリーボードで設定したセルのID
            testCell = whoAddChoseImageCollectionView.dequeueReusableCell(withReuseIdentifier: "whoAddChoseImageCell",for: indexPath)
            
//            // Tag番号を使ってImageViewのインスタンス生成
//            let imageView = testCell.contentView.viewWithTag(1) as! UIImageView
//            // 画像配列の番号で指定された要素の名前の画像をUIImageとする
//            let cellImage = UIImage(named: iconNames[indexPath.row])
//            // UIImageをUIImageViewのimageとして設定
//            imageView.image = cellImage
            
            let imageDisplay = testCell.contentView.viewWithTag(1)!
            
            svgFunc.drawSvgOnUIView(
                targetView:imageDisplay,
                svgPathData:iconData.pathDic[iconNames[indexPath.row]]!,
                svgColorHex:"4d4d4d")
            
            return testCell

        }else if( collectionView.tag == 2 ){
            // カラー選択コレクションビュー(タグ2)
            testCell = whoAddChoseColorCollectionView.dequeueReusableCell(withReuseIdentifier: "whoAddChoseColorCell",for: indexPath)
            let cellColor: String = colorHex[indexPath.row]
            
            testCell.backgroundColor = UIColor(hex: "\(cellColor)")
            
            return testCell
        }else{
        }
        
        // なんかとりあえずif文の外に戻り値を返さないといけないので書いてる。
        
        return testCell
        
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // section数は１つ
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        // 要素数を入れる、要素以上の数字を入れると表示でエラーとなる
        
        // 返すアイテムの数
        var items_count = 0;

        if(collectionView.tag == 1 ){
            // 画像選択コレクションビュー（タグ1）
            items_count = iconNames.count;
            
        }else if(collectionView.tag == 2){
            // カラー選択コレクションビュー(タグ2)
            items_count = colorHex.count;
        }else{
            // 上記の条件にどれも該当しない場合
            items_count = 0;
        }
        
        
        return items_count;
    }
    
    // Cell がタップされた場合
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        
        if( collectionView.tag == 1 ){
            // セルの番号と同じ画像名をすでに定義されているselectedImageに格納
            selectedImage = "\(iconNames[indexPath.row])"
            
            svgFunc.drawSvgOnUIView(
                targetView:iconPreView,
                svgPathData:iconData.pathDic[selectedImage]!,
                svgColorHex:selectedColor)
            
        }else if( collectionView.tag == 2 ){
            // セルの番号と同じhexをすでに定義されているselectedColorに格納
            selectedColor = "\(colorHex[indexPath.row])"
            
            svgFunc.drawSvgOnUIView(
                targetView:iconPreView,
                svgPathData:iconData.pathDic[selectedImage]!,
                svgColorHex:selectedColor)
        }else{}
    }
    
        
    
    
    
    
    //追加ボタン
    @IBAction func addWhoRegister(_ sender: Any) {
        
        
        if(whereComeFrom == ""){
            
            //Realmオブジェクト生成
            let realm = try! Realm()
            
            //WhoDataのリスト順のラストを特定
            let lastPerson = realm.objects(WhoData.self).sorted(byKeyPath:"listNum").last
            
            
            var latestListNum = Int()
            
            if (lastPerson == nil){
                //まだ誰も登録されていない状態
                
                latestListNum = 0
                
            }else{
                latestListNum = lastPerson!.listNum + 1
            }
            
            //メンバーデータを定義
            let newMember = WhoData()
            
            newMember.idByTime = NSDate().timeIntervalSince1970
            newMember.listNum = latestListNum
            newMember.name = addWhoName.text!
            newMember.iconImage = selectedImage
            newMember.iconColor = selectedColor
            try! realm.write {
                realm.add(newMember)
            }
       
        }else if(whereComeFrom == "personListForEditView"){
            
            //Realmオブジェクト生成
            let realm = try! Realm()
            try! realm.write {
                whoToEdit.name = addWhoName.text!
                whoToEdit.iconImage = selectedImage
                whoToEdit.iconColor = selectedColor
            }
            
            let preViewController = self.presentingViewController as! personListForEditViewController
            preViewController.editPersonCollectionView.reloadData()
        }
        
        
        dismiss(animated: true, completion: nil)


        

    }
    
    @IBAction func cancelButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    //ジェノサイドボタン
    @IBAction func genocideButton(_ sender: Any) {
        //Realmオブジェクト生成
        let realm = try! Realm()
        let members = realm.objects(WhoData.self).filter("idByTime != 0").sorted(byKeyPath:"idByTime") //WhoDataのidByTime(not0)でsortしたものをmembersと定義
        let thanks = realm.objects(thanksData.self).sorted(byKeyPath:"thanksIdByTime")
        try! realm.write {
            realm.delete(members)
            realm.delete(thanks)
        }
        print("皆殺し完了")
    }
    
    @IBAction func birthButton(_ sender: UIButton) {
        
        birthThanksForDebug()
    }
    
    func birthThanksForDebug () {
        
        // コレクション
        //20
        var arrayName = ["マーティ","ドク", "スタローン", "シュワ", "ブルース","ロッキー","ランボー","メルギブ","ナポレオン","ペドロ","サイモン","ニック","エドガー","ダガード","クリリン","吉良吉影","スパイダーマン","アイアンマン","ハルク","キャップ","ソー","ホームズ","ルーク","オビワン","アポロ","ジャッキー","李小龍","ユンピョウ","サモハン","トニージャー","ジージャー","クリント"]
        
        //キャラを20体つくる
        for times in 1...20 {
            let randomIndexName = Int(arc4random_uniform(UInt32(arrayName.count - 1)))
            let randomValueName = arrayName[randomIndexName]
            arrayName.remove(at: randomIndexName)
            
            let randomIndexIcon = Int(arc4random_uniform(UInt32(iconNames.count)))
            let randomValueIcon = iconNames[randomIndexIcon]
            
            let randomIndexColor = Int(arc4random_uniform(UInt32(colorHex.count)))
            let randomValueColor = colorHex[randomIndexColor]
            
            //Realmオブジェクト生成
            let realm = try! Realm()
            
            //WhoDataのリスト順のラストを特定
            let lastPerson = realm.objects(WhoData.self).sorted(byKeyPath:"listNum").last
            
            
            var latestListNum = Int()
            
            if (lastPerson == nil){
                //まだ誰も登録されていない状態
                
                latestListNum = 0
                
            }else{
                latestListNum = lastPerson!.listNum + 1
            }

            
            let randomMember = WhoData()
            
            randomMember.idByTime = NSDate().timeIntervalSince1970 + Double(times) * 0.1
            randomMember.listNum = latestListNum
            randomMember.name = randomValueName
            randomMember.iconImage = randomValueIcon
            randomMember.iconColor = randomValueColor
            try! realm.write {
                realm.add(randomMember)
            }
            
        }
        
        let thanksEventSample:[String] = ["アメくれた",
                                 "かわいがってくれた",
                                 "叱ってくれた",
                                 "パンかってくれた",
                                 "英語おしえてくれた",
                                 "荷物持ってくれた",
                                 "忙しいのに遊んでくれた",
                                 "心配してくれた",
                                 "話しかけてくれた",
                                 "終わるの待っててくれた",
                                 "ラーメンおごってくれた",
                                 "邪魔なやつを消してくれた",
                                 ]
        
        let thanksYearSample:[Int] = [1999,645,794,2017,2016,2015,1600,2010]
        
        
        let realm = try! Realm()
        
        for times in 1...400 {
            let randomIndexYear:Int = Int(arc4random_uniform(UInt32(thanksYearSample.count)))
            let randomYear:Int = thanksYearSample[randomIndexYear]
            
            let randomMonth:Int = Int(arc4random_uniform(UInt32(12)))
            
            let randomDay:Int = Int(arc4random_uniform(UInt32(31)))
            
            let personCount:Int = realm.objects(WhoData.self).count
            let randomIndexPerson:Int = Int(arc4random_uniform(UInt32(personCount)))
            let randomPerson:Double = realm.objects(WhoData.self)[randomIndexPerson].idByTime
            
            let randomIndexEvent:Int = Int(arc4random_uniform(UInt32(thanksEventSample.count)))
            let randomEvent:String = thanksEventSample[randomIndexEvent]
            
            //本当は2択だけど、4択にして偏りを持たせる。
            let randomIndexCheck:Int = Int(arc4random_uniform(UInt32(4)))
            var randomCheck = Bool()
            if (randomIndexCheck == 0) {
                randomCheck = true
            }else{
                randomCheck = false
            }
            
            let newThanks = thanksData()
            
            newThanks.thanksIdByTime = NSDate().timeIntervalSince1970 + Double(times) * 0.1
            newThanks.thanksDateInt = randomYear * 10000 + randomMonth * 100 + randomDay
            newThanks.thanksDateY = randomYear
            newThanks.thanksDateM = randomMonth
            newThanks.thanksDateD = randomDay
            newThanks.thanksDateWD = "サンプル"
            newThanks.thanksPerson = randomPerson
            newThanks.thanksEvent = randomEvent
            newThanks.thanksCheck = randomCheck
            try! realm.write {
                realm.add(newThanks)
            }

            
        }
        
                
    }

    
    
    

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
