//
//  ViewController.swift
//  memorealm
//
//  Created by a_b_c on 2017/10/15.
//  Copyright © 2017年 konjo_ron. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftSVG
import GoogleMobileAds

class User: Object {
    dynamic var id = 0
    dynamic var name = ""
    dynamic var createdAt: Double = 0
}

class WhoData: Object {
    //id 作成時で管理
    dynamic var idByTime: Double = 0
    //リスト番号
    dynamic var listNum: Int = 0
    //名前
    dynamic var name:String = ""
    //iconナンバー
    dynamic var iconImage:String = ""
    //iconカラー
    dynamic var iconColor:String = ""
    
}

class thanksData: Object {
    //id 作成時で管理
    dynamic var thanksIdByTime:Double = 0
    //日付数字列
    dynamic var thanksDateInt: Int = 0
    //日付：年
    dynamic var thanksDateY:Int = 0
    //日付：月
    dynamic var thanksDateM:Int = 0
    //日付：日
    dynamic var thanksDateD:Int = 0
    //日付：曜日
    dynamic var thanksDateWD:String = ""
    //誰の（WhoDataのidByTime）
    dynamic var thanksPerson: Double = 0
    //イベント
    dynamic var thanksEvent: String = ""
    //ありがとチェック
    dynamic var thanksCheck: Bool = false
    
}


// project全体で使えるように


struct colors {
    
    static let checkedHeart:String = "c7243a"
    static let unCheckedHeart:String = "cccccc"
    
}






class ViewController: UIViewController ,UICollectionViewDataSource,UICollectionViewDelegate,UITextFieldDelegate{
    
    
    //デートピッカー用のやつ
    
    //dateformatterに名付け
    let dateFormatter = DateFormatter()
    //デートピッカーを命名
    let inputDatePicker = UIDatePicker()
    
    //カレンダー情報を変数定義
    var calendar = Calendar.current
    
    //曜日の日本語名表示
    let weekdayJapArray:[String] = ["日","月","火","水","木","金","土"]
    
    //データ用日付格納変数
    var thanksDateInputInt:Int = 0
    var thanksDateInputY:Int = 0
    var thanksDateInputM:Int = 0
    var thanksDateInputD:Int = 0
    var thanksDateInputWD:String = ""
    
    
    //チェックされた人物の格納用
    var thanksPersonInput: Double = 0
    //人物を選んだかの判定用
    var thanksPersonInputCheck: Bool = false
    
    
    
    //ありがとチェックの判定データ
    var thanksCheckInput: Bool = false;
    
    
    
    
    //どこから来たか情報
    var whereComeFrom:String = ""
    //どのthanksを編集するか情報
    var selectedThanks = thanksData()
    
    
    
    
    //削除に使うためのCAShapeLayerの配列を設定
    var addedlayers = [CAShapeLayer]()
    
    
    
    
    
    
    @IBOutlet weak var dateInputTextField: UITextField!
    
    @IBOutlet weak var thanksEventTextField: UITextField!
    
    
    @IBOutlet weak var selectPersonCollectionView: UICollectionView!
    
    
    @IBOutlet weak var thanksCheckButton: UIButton!
     
    @IBOutlet weak var thanksCheckImage: UIImageView!
    
    @IBOutlet weak var thanksInputButton: UIButton!
    
    
    
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //デートピッカー用のもろもろ
        //現在の日付を定義
        let nowDate = Date()
        
        //カレンダーのロケールを端末のものにする
        calendar.locale = Locale.current
        
        
        
        
        let year:Int = calendar.component(.year, from: nowDate)
        let month:Int = calendar.component(.month, from: nowDate)
        let day:Int = calendar.component(.day, from: nowDate)
        let hour:Int = calendar.component(.hour, from: nowDate)
        let minute:Int = calendar.component(.minute, from: nowDate)
        print("端末のロケールチェック：現在の時間",hour,minute)
        
        //曜日の数字を取得
        let weekdayNum: Int = calendar.component(.weekday, from: nowDate)
        //曜日を日本語化
        let weekday:String = weekdayJapArray[weekdayNum - 1]
        
        //現在の日にちをStringにする
        let nowDateString:String = "\(year)年\(month)月\(day)日(\(weekday))"
        
        //デートフォーマットstring表示の形。これを設定しておかないと、表示されない。(不採用)
        //dateFormatter.dateFormat = "yyyy年MM月dd日"
        
        
        //デートピッカーのモードを年月日に
        inputDatePicker.datePickerMode = UIDatePickerMode.date
        //フィールド編集にデートピッカー使用
        dateInputTextField.inputView = inputDatePicker

        
        
        if(whereComeFrom == ""){
            
            
            //年月日曜日ををデータ作成用に格納
            thanksDateInputInt = (year * 10000) + (month * 100) + day
            thanksDateInputY = year
            thanksDateInputM = month
            thanksDateInputD = day
            thanksDateInputWD = weekday

            
            //textInputTextFieldにnowDateStringをぶちこむ
            dateInputTextField.text = nowDateString
            
            
            //ありがとチェックをfalseにしてグレーにする
            thanksCheckInput = false
            thanksCheckImage.tintColor = UIColor(hex: colors.unCheckedHeart)
        }else if(whereComeFrom == "thanksListViewForEditting"){
            
            //年月日曜日ををデータ作成用に格納
            thanksDateInputInt = (selectedThanks.thanksDateY * 10000) + (selectedThanks.thanksDateM * 100) + selectedThanks.thanksDateD
            thanksDateInputY = selectedThanks.thanksDateY
            thanksDateInputM = selectedThanks.thanksDateM
            thanksDateInputD = selectedThanks.thanksDateD
            thanksDateInputWD = selectedThanks.thanksDateWD
            
            
            //表示する日にちのString作成
            let currentDate:String = "\(selectedThanks.thanksDateY)年\(selectedThanks.thanksDateM)月\(selectedThanks.thanksDateD)日(\(selectedThanks.thanksDateWD))"
            //textInputTextFieldにcurrentDateをぶちこむ
            dateInputTextField.text = currentDate
            //thanksEventTextFieldにthanksEventをぶちこむ
            thanksEventTextField.text = selectedThanks.thanksEvent
            //人を設定する
            thanksPersonInput = selectedThanks.thanksPerson
            
            //ありがとチェック
            thanksCheckInput = selectedThanks.thanksCheck
            if(thanksCheckInput == true){
                thanksCheckImage.tintColor = UIColor(hex: colors.checkedHeart)
            }else{
                thanksCheckImage.tintColor = UIColor(hex: colors.unCheckedHeart)
            }
            
            
            
            //デートピッカーの初期位置を編集するthanks準拠に
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  "yyyy:MM:dd"
            
            print(selectedThanks.thanksDateM)
            print("\(selectedThanks.thanksDateY):\(selectedThanks.thanksDateM):\(selectedThanks.thanksDateD)")
            
            let selectedThanksDate = dateFormatter.date(from: "\(selectedThanks.thanksDateY):\(selectedThanks.thanksDateM):\(selectedThanks.thanksDateD)")
            inputDatePicker.date = selectedThanksDate!

            
            //決定ボタンを『変更』にする
            thanksInputButton.setTitle("変更", for: .normal)
        }

        
        
        // デートピッカーに表示するツールバーの表示
        let pickerToolBar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.size.height/6,width: self.view.frame.size.width,height: 40.0))
        pickerToolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
        pickerToolBar.isTranslucent = false
        pickerToolBar.tintColor = UIColor.white
        pickerToolBar.backgroundColor = UIColor(hex: "c7243a")
        
        //ボタンの設定
        //右寄せのためのスペース設定
        let spaceBarBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace,target: self,action: nil)
        
        //完了ボタンを設定
        let toolBarBtn = UIBarButtonItem(title: "完了", style: .done, target: self, action: #selector(ViewController.toolBarBtnPush))
        
        
        //ツールバーにボタンを表示
        pickerToolBar.items = [spaceBarBtn,toolBarBtn]
        dateInputTextField.inputAccessoryView = pickerToolBar
        
        
        
        
        
        // キーボードに表示するツールバーの表示
        let keyBoardToolBar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.size.height/6,width: self.view.frame.size.width,height: 40.0))
        keyBoardToolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
        keyBoardToolBar.isTranslucent = false
        keyBoardToolBar.tintColor = UIColor.white
        keyBoardToolBar.barTintColor = UIColor(hex: "c7243a")
        
        //ボタンの設定
        //右寄せのためのスペース設定
        //デートピッカーと共通なので省略
        
        //完了ボタンを設定
        let toolBarDoneButton = UIBarButtonItem(title: "完了", style: .done, target: self, action: #selector(ViewController.keyBoardToolBarBtnPush))
        
        
        //ツールバーにボタンを表示
        keyBoardToolBar.items = [spaceBarBtn,toolBarDoneButton]
        thanksEventTextField.inputAccessoryView = keyBoardToolBar

        
        
        
        
        
                
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        selectPersonCollectionView.reloadData()
        
        
        //アドセンス用
        var admobView = GADBannerView()
        
        admobView = GADBannerView(adSize:kGADAdSizeBanner)
        admobView.frame.origin = CGPoint(x:0, y:self.view.frame.size.height - admobView.frame.height - adsence.tabBarHeight)
        admobView.frame.size = CGSize(width:self.view.frame.width, height:admobView.frame.height)
        
        if adsence.AdMobTest {
            admobView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        }
        else{
            admobView.adUnitID = adsence.AdMobID
        }
        
        admobView.rootViewController = self
        admobView.load(GADRequest())
        
        self.view.addSubview(admobView)
        
        print("タブバーサイズ",adsence.tabBarHeight)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //デートピッカーの完了ボタンを押した時の処理
    func toolBarBtnPush(sender: UIBarButtonItem){
        
        // デートピッカーのDate()情報を定義
        let pickerDate = inputDatePicker.date
        
        calendar.locale = Locale.current
        
        let year:Int = calendar.component(.year, from: pickerDate)
        let month:Int = calendar.component(.month, from: pickerDate)
        let day:Int = calendar.component(.day, from: pickerDate)
        let hour:Int = calendar.component(.hour, from: pickerDate)
        let minute:Int = calendar.component(.minute, from: pickerDate)
        let weekNum:Int = calendar.component(.weekday, from: pickerDate)
        let weekday:String = weekdayJapArray[weekNum - 1]
        
        print("端末のロケールチェック：現在の時間",hour,minute)
        
        let dateDisplay:String = "\(year)年\(month)月\(day)日(\(weekday))"
        
        
        //フィールドに値を入力
        dateInputTextField.text = dateDisplay
        
        //年月日曜日ををデータ作成用に格納
        thanksDateInputInt = (year * 10000) + (month * 100) + day
        thanksDateInputY = year
        thanksDateInputM = month
        thanksDateInputD = day
        thanksDateInputWD = weekday
        
        //デートピッカーを閉じる
        self.view.endEditing(true)
    }
    
    //キーボードの完了ボタンを押した時の処理
    func keyBoardToolBarBtnPush(sender: UIBarButtonItem){
        
        //キーボードを閉じる
        self.view.endEditing(true)
    }


   
    @IBAction func thanksEventTextField(_ sender: Any) {
        
    }
    
    
        
    // cell表示用
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        

        // cellをtestCellと定義
        let testCell:UICollectionViewCell =
            collectionView.dequeueReusableCell(withReuseIdentifier: "whoCell",
                                               for: indexPath)

        if (indexPath.row == 0){
            // Tag番号を使ってLabelのインスタンス生成 cell内のlabelを定義
            let nameDisplay = testCell.contentView.viewWithTag(2) as! UILabel
            nameDisplay.text = "人を追加"
            
            let imageDisplay = testCell.contentView.viewWithTag(1)!
            
            svgFunc.drawSvgOnUIView(
                targetView:imageDisplay,
                svgPathData:pathDatas.personPlusIcon,
                svgColorHex:"4d4d4d")
            
        }else{
            let realm = try! Realm()
            //WhoDataをlistNumでソート
            let members = realm.objects(WhoData.self).sorted(byKeyPath:"listNum")
            // cellの順番にあたるものを抜き出してmemberと定義
            let member = members[indexPath.row - 1]
            
            
            // Tag番号を使ってLabelのインスタンス生成 cell内のlabelを定義
            let nameDisplay = testCell.contentView.viewWithTag(2) as! UILabel
            nameDisplay.text = member.name
            
            let imageDisplay = testCell.contentView.viewWithTag(1)!
            
            svgFunc.drawSvgOnUIView(
                targetView:imageDisplay,
                svgPathData:iconData.pathDic[member.iconImage]!,
                svgColorHex:member.iconColor)
        
        }
        
        
        return testCell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // section数は１つ
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        // 要素数を入れる、要素以上の数字を入れると表示でエラーとなる
        
        // メンバーの数をカウントして、セルの数をその数に設定する
        let realm = try! Realm()
        let members = realm.objects(WhoData.self)
        
        //プラス1は人追加ボタンのため。
        
        return members.count + 1;
    }
    
    // Cell がタップされた場合
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        
        // cellをtestCellと定義
        let testCell:UICollectionViewCell = collectionView.cellForItem(at: indexPath)!
        
        if (indexPath.row == 0){
            
            self.performSegue(withIdentifier: "toPersonAddView", sender: nil)
            
        }else{
            
            testCell.backgroundColor = UIColor(hex: "ffcccc")
            
            let realm = try! Realm()
            
            //idByTimeで整列
            let members = realm.objects(WhoData.self).sorted(byKeyPath:"listNum")
            
            //その中でタップされたcellのものをmemberと定義
            let member = members[indexPath.row - 1]
            
            //そいつのidByTimeをthanksPersonに入れる
            thanksPersonInput = member.idByTime
            
            //人物を選んだかにチェック
            thanksPersonInputCheck = true

        }
        
    }
    
    //セルの選択が解除されたとき
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let testCell:UICollectionViewCell = collectionView.cellForItem(at: indexPath)!
        testCell.backgroundColor = UIColor(hex: "ffffff")
        //バグ：コレクションビューをスクロールしてこの動作が行われるとクラッシュ
        //おそらく、画面外のものを戻そうとするとnilとなる。
        //nilでなければ。みたいなifをつければいいのかな？
    }

    
    
    
    
    //ありがとチェックボタンを押した時
    @IBAction func thanksCheckButton(_ sender: UIButton) {
        
        if (thanksCheckInput == false){
            //ありがとチェックなしの場合
            //trueにしてハートを赤くする
            thanksCheckInput = true
            thanksCheckImage.tintColor = UIColor(hex: colors.checkedHeart)
        }else{
            //ありの場合
            //falseにしてグレーにする
            thanksCheckInput = false
            thanksCheckImage.tintColor = UIColor(hex: colors.unCheckedHeart)
        }
        
    }
    
    
    
    @IBAction func thanksInputButton(_ sender: UIButton) {
        //Realmオブジェクト生成
        let realm = try! Realm()
        
        if(whereComeFrom == ""){
            if (thanksPersonInputCheck == true){
                
                let newThanks = thanksData()
                
                newThanks.thanksIdByTime = NSDate().timeIntervalSince1970
                newThanks.thanksDateInt = thanksDateInputInt
                newThanks.thanksDateY = thanksDateInputY
                newThanks.thanksDateM = thanksDateInputM
                newThanks.thanksDateD = thanksDateInputD
                newThanks.thanksDateWD = thanksDateInputWD
                newThanks.thanksPerson = thanksPersonInput
                newThanks.thanksEvent = thanksEventTextField.text!
                newThanks.thanksCheck = thanksCheckInput
                try! realm.write {
                    realm.add(newThanks)
                }
                
            }else{
                print("エラー：人を選んでいないのよ")
            }

        }else if(whereComeFrom == "thanksListViewForEditting") {
            
            try! realm.write() {
                selectedThanks.thanksIdByTime = NSDate().timeIntervalSince1970
                selectedThanks.thanksDateInt = thanksDateInputInt
                selectedThanks.thanksDateY = thanksDateInputY
                selectedThanks.thanksDateM = thanksDateInputM
                selectedThanks.thanksDateD = thanksDateInputD
                selectedThanks.thanksDateWD = thanksDateInputWD
                selectedThanks.thanksPerson = thanksPersonInput
                selectedThanks.thanksEvent = thanksEventTextField.text!
                selectedThanks.thanksCheck = thanksCheckInput
            }
            
            dismiss(animated: true, completion: nil)

        }

    }
    
    
    
    
   

}

//カラーコードを使用可能に
extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}
//使用例　let color = UIColor(hex: "C7243A")


struct svgFunc {
    
     static func drawSvgOnUIView(targetView:UIView,svgPathData:String,svgColorHex:String){
        
        
        //まずは、ターゲットビューの中のタグ100のビューを消す。最初から用意しておくこと。
        let delview = targetView.viewWithTag(100)
        delview?.removeFromSuperview()
        
        //新たにビューを用意
        let newView = UIView()
        //注）タグ1をつける。新規にのせる時に古いのを消すので。
        newView.tag = 100
        newView.frame = CGRect(x:0,y:0,width:targetView.bounds.width,height:targetView.bounds.height)
        //ターゲットビューの下に設置する。
        targetView.addSubview(newView)
        
        let targetTag1View = targetView.viewWithTag(100)!
        
        //目的のpathデータをUIBezierPathの形式にして定義
        let currentPath = UIBezierPath(pathString: svgPathData)
        
        
        
        //pathデータを完パケ（.cgPath）したもののboundingBox = CGRect(x,y,width,height)をcurrentPathCGRectと定義
        let currentPathCGRect = currentPath.cgPath.boundingBox
        
        //パスの縦長度(傾きy/x)を計測
        let currentPathAspectRatio = currentPathCGRect.height/currentPathCGRect.width
        //ビューの縦長度(傾きy/x)を計測
        let viewAspectRatio = targetTag1View.bounds.height/targetTag1View.bounds.width
        
        let scaleFactor: CGFloat
        let XcgFloatForCentering: CGFloat
        let YcgFloatForCentering: CGFloat
        if (currentPathAspectRatio > viewAspectRatio) {
            //パスの方が縦長だったら、縦の比率をゲットする。
            scaleFactor = targetTag1View.bounds.height / currentPathCGRect.height
            
            //センタリング用の値を設定。『ビューのwidthに逆スケール』からパスのwidthを引いて割る2
            //逆スケールをかけているのは、パスの方に合わせて値をとっておくため。
            //このあと、パスを移動してからスケーリングするのでそのときのため。
            XcgFloatForCentering = ((targetTag1View.bounds.width / scaleFactor) - currentPathCGRect.width) / 2
            YcgFloatForCentering = 0
        } else {
            //ビューの方が縦長だったら、縦の比率をゲットする。
            scaleFactor = targetTag1View.bounds.width  / currentPathCGRect.width
            XcgFloatForCentering = 0
            YcgFloatForCentering = ((targetTag1View.bounds.height / scaleFactor) - currentPathCGRect.height) / 2
        }
        
        //パスの(x,y)に対して(-x,-y)平行移動して、設定しておいた値を加える
        let transX = -1 * currentPathCGRect.minX + XcgFloatForCentering
        let transY = -1 * currentPathCGRect.minY + YcgFloatForCentering
        
        //パスの左上ポイントを変更
        var affineTransform1 = CGAffineTransform(translationX: transX, y: transY)
        let transformedPath1 = currentPath.cgPath.copy(using: &affineTransform1)
        
        //次に、取得しておいたスケール分に拡大ないし縮小
        var affineTransform2 = CGAffineTransform(scaleX: scaleFactor, y: scaleFactor)
        let transformedPath = transformedPath1!.copy(using: &affineTransform2)
        
        
        //変形が終わったパスをレンダリングして色を変え、レイヤーに出力
        let currentShape = CAShapeLayer()
        currentShape.path = transformedPath
        currentShape.fillColor = UIColor(hex: svgColorHex).cgColor
        targetTag1View.layer.addSublayer(currentShape)
    }

}




//パスデータ。実験用
struct pathDatas {
    
    static let mummy = "M503.05,497.319c-0.116-0.69-4.456-32.903-20.451-73.094c-14.982-37.525-40.434-82.23-82.988-113.898 c11.81-29.109,18.27-64.066,20.509-101.834c0.37-6.547,0.564-13.027,0.564-19.439c-0.02-51.36-12.337-98.273-39.636-133.063 c-13.621-17.348-31.036-31.513-52.011-41.164C308.08,5.157,283.66-0.009,256.088,0c-27.572-0.009-51.991,5.157-72.968,14.837 c-31.502,14.477-54.794,39.111-69.718,69.417c-14.963,30.335-21.91,66.284-21.928,104.85c0,6.392,0.194,12.862,0.584,19.38 c2.334,39.568,8.522,74.447,21.345,104.159c-42.38,31.542-68.025,75.595-83.241,112.546 c-16.286,39.626-20.917,71.304-21.015,71.994L6.986,512h498.029L503.05,497.319z M474.602,486.102H37.749 c1.46-6.284,3.542-14.244,6.382-23.359c23.194,2.607,46.933,3.794,70.672,3.794c127.255-0.029,252.799-31.746,271.089-36.522 c24.868-0.846,47.497-4.086,66.371-10.867c2.258,4.952,4.359,9.865,6.266,14.662c7.433,18.621,12.181,35.54,15.041,47.712 C473.94,483.135,474.291,484.662,474.602,486.102z M145.566,317.294c-0.623-0.302-1.071-0.535-1.655-0.818l-2.802-5.535 c-7.978-15.732-13.66-34.266-17.551-55.29c0.272,0,0.466,0.01,0.739,0.01c5.857-0.01,13.251-0.244,21.638-0.934 c14.087,5.185,104.315,37.349,194.58,37.408c13.096,0,26.21-0.72,39.091-2.286c-2.296,6.83-4.825,13.3-7.628,19.225l-4.806,10.157 l9.379,6.188c9.36,6.159,17.804,13.241,25.607,20.791c-29.557,4.932-57.246,7.141-82.619,7.141 c-58.412,0-105.17-10.722-137.237-21.413C166.25,326.594,153.894,321.262,145.566,317.294z M132.042,106.232 c42.614,4.904,166.23,21.19,261.05,52.819c0.311,2.909,0.603,5.847,0.817,8.815H118.266 C120.038,145.265,124.629,124.474,132.042,106.232z M384.044,275.167c-14.186,2.052-28.799,3.026-43.528,3.026 c-47.245,0.009-95.267-9.291-131.381-18.622c-12.492-3.23-23.446-6.431-32.572-9.271c25.957-5.458,55.727-16.394,82.502-37.642 c4.826,1.051,9.244,3.814,13.698,7.083c3.658,3.171,7.686,6.129,12.045,8.883c2.219,1.508,4.534,2.909,6.986,4.115 c10.254,5.623,21.716,10.322,33.487,14.087c21.054,6.675,42.847,10.352,59.93,10.371c1.032,0,1.908-0.116,2.9-0.145 C386.904,263.278,385.582,269.368,384.044,275.167z M304.246,211.791c-11.014,0-19.925-8.921-19.925-19.925h39.85 C324.172,202.87,315.24,211.791,304.246,211.791z M224.546,191.866c0,11.004-8.932,19.925-19.925,19.925 c-11.014,0-19.925-8.921-19.925-19.925H224.546z M394.785,189.055c0,5.886-0.175,11.87-0.526,17.931 c-0.719,12.502-2.023,24.546-3.755,36.095c-1.674,0.106-3.405,0.174-5.293,0.174c-15.099,0.01-35.958-3.434-55.708-9.728 c-0.914-0.292-1.828-0.623-2.742-0.925c2.49-1.041,5.059-2.286,7.782-3.795c15.625-8.687,19.439-24.458,19.809-36.571 c0.116-4.028-1.576-7.598-4.32-10.42h44.598C394.687,184.229,394.785,186.613,394.785,189.055z M159.654,181.816 c-2.763,2.822-4.436,6.392-4.32,10.42c0.351,12.113,4.184,27.884,19.808,36.571c3.834,2.131,7.356,3.63,10.741,4.855 c-24.945,6.772-47.653,8.076-61.584,8.047c-1.09,0-2.004-0.02-2.997-0.03c-1.537-10.974-2.685-22.503-3.385-34.703 c-0.351-6.041-0.545-12.005-0.545-17.872c0-2.472,0.175-4.845,0.233-7.288H159.654z M390.815,143.65 c-54.19-17.434-115.931-30.004-166.969-38.507c38.546-12.94,73.376-21.492,99.917-26.882c15.254-3.114,27.669-5.166,36.62-6.509 c5.74,7.258,10.818,15.216,15.157,23.953C382.565,109.928,387.624,126.099,390.815,143.65z M151.462,72.015 c11.304-14.341,25.295-25.714,42.515-33.662c17.201-7.929,37.729-12.444,62.11-12.453c24.381,0.009,44.89,4.524,62.11,12.453 c11.753,5.438,21.987,12.56,30.939,21.063c-31.445,5.156-88.924,16.87-154.284,41.134c-23.427-3.522-43.099-6.032-56.662-7.638 C142.103,85.412,146.461,78.358,151.462,72.015z M135.992,328.112c23.681,11.811,88.69,39.17,183.548,39.188 c28.74,0,60.301-2.675,94.119-8.912c13.193,15.138,23.914,31.707,32.437,48.198c-16.754,5.778-37.886,8.785-61.449,9.534 l-0.059-0.194c0,0-0.35,0.097-0.875,0.233c-3.834,0.116-7.745,0.165-11.714,0.165c-61.079,0.01-135-13.114-193.452-26.297 c-29.226-6.597-54.6-13.184-72.676-18.126c-4.923-1.352-9.243-2.558-12.998-3.628C104.88,353.3,119.044,339.387,135.992,328.112z M83.864,380.211c16.404,4.728,50.105,14.058,91.628,23.418c46.408,10.458,102.369,20.84,154.846,24.838 c-51.699,10.624-133.579,24.127-215.536,24.118c-22.28,0-44.5-1.071-66.119-3.376c2.024-5.653,4.281-11.548,6.869-17.638 C62.616,414.914,71.975,397.159,83.864,380.211z"
    
    static let standard = "M47.984,62.461L16.016,62.461C16.794,58.942 18.156,55.777 19.944,53.187C10.911,48.73 4.691,39.426 4.691,28.68C4.691,13.608 16.928,1.371 32,1.371C47.072,1.371 59.309,13.608 59.309,28.68C59.309,39.426 53.089,48.73 44.056,53.187C45.844,55.777 47.206,58.942 47.984,62.461ZM38.653,38.734C37.138,40.834 34.677,42.141 32,42.141C29.327,42.141 26.863,40.833 25.345,38.733C24.933,38.163 24.138,38.035 23.569,38.447C22.998,38.859 22.871,39.654 23.282,40.224C25.269,42.975 28.49,44.686 32,44.686C35.513,44.686 38.732,42.974 40.718,40.223C41.129,39.653 41.001,38.858 40.431,38.447C39.861,38.035 39.065,38.163 38.653,38.734ZM21.758,30.467C21.758,29.128 20.673,28.043 19.333,28.043C17.995,28.043 16.909,29.128 16.909,30.467C16.909,31.805 17.995,32.891 19.333,32.891C20.673,32.891 21.758,31.805 21.758,30.467ZM44.667,28.043C43.328,28.043 42.242,29.128 42.242,30.467C42.242,31.805 43.328,32.891 44.667,32.891C46.006,32.891 47.091,31.805 47.091,30.467C47.091,29.128 46.006,28.043 44.667,28.043Z"
    
    
    static let personPlusIcon = "M45.373,64.025L11.869,64.025C12.684,60.337 14.112,57.02 15.985,54.305C6.519,49.635 0,39.883 0,28.621C0,12.825 12.825,0 28.621,0C44.418,0 57.242,12.825 57.242,28.621C57.242,39.883 50.724,49.635 41.257,54.305C43.131,57.02 44.558,60.336 45.373,64.025ZM31.372,14.082L25.87,14.082C25.045,14.082 24.377,14.75 24.377,15.574L24.377,23.531C24.377,24.355 23.709,25.024 22.884,25.024L14.927,25.024C14.102,25.025 13.434,25.693 13.434,26.518L13.434,32.02C13.434,32.844 14.102,33.513 14.927,33.513L22.884,33.513C23.709,33.513 24.377,34.181 24.377,35.006L24.377,42.962C24.377,43.787 25.045,44.455 25.87,44.455L31.372,44.455C32.197,44.455 32.865,43.787 32.865,42.962L32.865,35.006C32.865,34.181 33.534,33.513 34.358,33.513L42.315,33.513C43.139,33.513 43.808,32.844 43.808,32.02L43.808,26.518C43.808,25.693 43.139,25.024 42.315,25.024L34.358,25.024C33.534,25.024 32.865,24.356 32.865,23.531L32.865,15.574C32.865,14.75 32.197,14.082 31.372,14.082Z"

    
    
}


